﻿interface IRunner 
{
	float Speed {get;set;}
	float ReactionTimer {get;set;}
    float BoostAmount { get; set; }
    bool Drifting { get; set; }
    bool Boosting { get; set; }
    float CurrentSpeed { get; set; }
    float DriftSpeed { get; set; }
    void Move();
    void Boost();
}
