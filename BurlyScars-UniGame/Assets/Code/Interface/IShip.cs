﻿interface IShip
{
    float MoveSpeed { get; set; }
    float BoostSpeed { get; set; }
    int ShipLevel { get; set; }
    void Death();
    void Move();
    void Boost();
}
