﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ShipBase : MonoBehaviour, IShip
{
    #region Serialized Floats
    [SerializeField]
    private float _moveSpeed;
    [SerializeField]
    private float _boostSpeed;
    [SerializeField]
    private int _shipLevel;
    #endregion
    #region Floats
    public float MoveSpeed
    {
        get { return _moveSpeed; }
        set { _moveSpeed = value; }
    }

    public float BoostSpeed
    {
        get { return _boostSpeed; }
        set { _boostSpeed = value; }
    }

    public int ShipLevel
    {
        get { return _shipLevel; }
        set { _shipLevel = value; }
    }
#endregion

    private Rigidbody _rigidbody;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Move();
    }

    public void Death()
    {
        //throw new System.NotImplementedException();
    }

    public void Move()
    {
        //throw new System.NotImplementedException();
    }

    public void Boost()
    {
        //throw new System.NotImplementedException();
    }
}