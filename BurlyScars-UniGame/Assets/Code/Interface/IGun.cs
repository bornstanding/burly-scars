﻿interface IGun
{
    float ResetShotTime { get; set; }
    float ShotTime { get; set; }
    void FireOneShot();
}