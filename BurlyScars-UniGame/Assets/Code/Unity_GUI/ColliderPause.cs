﻿using UnityEngine;
using System.Collections;

public class ColliderPause : MonoBehaviour 
{
	void OnMouseDown()
	{
		if(Time.timeScale == 0)
		{
			Time.timeScale = 1;
		}

		else if(Time.timeScale == 1)
		{
			Time.timeScale = 0;
		}
	}
}
