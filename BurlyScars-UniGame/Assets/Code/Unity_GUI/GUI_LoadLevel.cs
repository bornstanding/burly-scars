﻿using UnityEngine;
using System.Collections;

public class GUI_LoadLevel : MonoBehaviour 
{

	//public Rect myButtonRectangle = new Rect (10, 10, 150, 100);
	public int numberOfButtons;
	public int spaceBetweenButtons;
	public string levelName;
	public float widthSize;
	public float heightSize;
	public float widthAmount;
	public float heightAmount;
	
	void OnGUI()
	{
		for(int i = 0; i < numberOfButtons; i++)
		{
			Rect newRect = new Rect (Screen.width/2 + widthAmount, Screen.height/2 + heightAmount, widthSize, heightSize);
			newRect.x += (spaceBetweenButtons + newRect.width) * i;
			
			if(GUI.Button(new Rect (newRect), "Menu"))
			{
				Application.LoadLevel(levelName);
			}
		}
	}
}
