﻿using UnityEngine;
using System.Collections;

public class LabelStandardHealth : MonoBehaviour 
{
	public StandardHealth standardHealth;

	void OnGUI() 
	{
		GUI.Label(new Rect(10, 10, 100, 20), "Player Health: " + standardHealth.currentHealth);
	}
}
