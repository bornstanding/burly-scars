﻿using UnityEngine;
using System.Collections;

public class CounterEventsThree : MonoBehaviour 
{
	public float currentCounter;
	public float minCounter;
	public float mediumCounter;
	public float maxCounter;
	
	public event EventHandlers.MinimalHandler MinCounterEvent = delegate { };
	public event EventHandlers.MinimalHandler MediumCounterEvent = delegate { };
	public event EventHandlers.MinimalHandler MaxCounterEvent = delegate { };
	
	void Update()
	{
		if(currentCounter == minCounter)
		{
			MinCounterEvent();
		}
		
		if(currentCounter == mediumCounter)
		{
			MediumCounterEvent();
		}
		
		if(currentCounter == maxCounter)
		{
			MaxCounterEvent();
		}
	}
}
