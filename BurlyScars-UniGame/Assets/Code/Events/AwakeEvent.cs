using UnityEngine;
using System.Collections;

public class AwakeEvent : MonoBehaviour
{
	public event EventHandlers.MinimalHandler Awaken = delegate { };

	void Awake()
	{
		Awaken();
	}
}
