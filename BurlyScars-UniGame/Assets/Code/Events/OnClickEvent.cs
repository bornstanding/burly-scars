﻿using UnityEngine;
using System.Collections;

public class OnClickEvent : MonoBehaviour 
{
    public event EventHandlers.MinimalHandler ClickedEvent = delegate { };
    public event EventHandlers.MinimalHandler UnClickedEvent = delegate { };

    private void OnMouseDown()
    {
        ClickedEvent();
    }

    private void OnMouseUp()
    {
        UnClickedEvent();
    }
}
