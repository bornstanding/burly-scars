using UnityEngine;
using System.Collections;

public class TimerCountDown : MonoBehaviour 
{
    public float timer;
	public float restartTimer;
	
	public event EventHandlers.MinimalHandler TimerRanOut = delegate { };
	
	void Update()
	{
		timer -= 1 * Time.deltaTime;
		if(timer <= 0)
		{
			timer = restartTimer;
			TimerRanOut();
		}
	}
}
