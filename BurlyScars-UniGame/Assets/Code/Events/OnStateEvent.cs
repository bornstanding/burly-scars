using UnityEngine;
using System.Collections;

public class OnStateEvent : StateBase
{
	public event EventHandlers.MinimalHandler EnteredState = delegate { };
	public event EventHandlers.MinimalHandler ExitState = delegate { };
	public event EventHandlers.MinimalHandler UpdateState = delegate { };
	
	public override void OnStateEnter (Object sender)
	{
		EnteredState();
	}
	
	public override void OnStateExit (Object sender)
	{
		ExitState();
	}
	
	public override void OnStateUpdate (Object sender)
	{
	    UpdateState();
	}
}
