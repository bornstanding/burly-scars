using UnityEngine;
using System.Collections;

public class OnCollisionEventsTag : MonoBehaviour 
{
	public string tagged;
	
	public event EventHandlers.MinimalHandler CollidedWithTheTag = delegate { };
	public event EventHandlers.MinimalHandler LeftTheTag = delegate { };
	public event EventHandlers.MinimalHandler UpdateTheCollisionWithTag = delegate { };
	
	void OnCollisionEnter(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			CollidedWithTheTag();
		}
	}
	
	void OnCollisionExit(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			LeftTheTag();
		}
	}
	
	void OnCollisionStay(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			UpdateTheCollisionWithTag();
		}
	}
}
