﻿using UnityEngine;
using System.Collections;

public class EventLightCollision : MonoBehaviour
{
    public delegate void AttackPlayer(Collider otherThing);

    public static event AttackPlayer attackPlayer;

    void OnTriggerStay(Collider otherThing)
    {
        LightElement otherLight = otherThing.gameObject.GetComponent<LightElement>();
        if (otherLight != null)
        {
            attackPlayer(otherThing);
        }
    }
}
