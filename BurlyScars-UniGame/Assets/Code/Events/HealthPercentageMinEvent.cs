﻿using UnityEngine;
using System.Collections;

public class HealthPercentageMinEvent : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public float currentPercent;
	public float minPercentage;
	
	public event EventHandlers.MinimalHandler MinPercentageEvent = delegate { };
	
	void Update()
	{
		currentPercent = (standardHealth.currentHealth/standardHealth.maxHealth) * 100;
		if(currentPercent <= minPercentage)
		{
			MinPercentageEvent();
		}
	}
	
}
