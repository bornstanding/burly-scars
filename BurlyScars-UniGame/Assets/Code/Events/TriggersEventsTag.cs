using UnityEngine;
using System.Collections;

public class TriggersEventsTag : MonoBehaviour 
{
	public string tagged;
	
	public event EventHandlers.MinimalHandler TriggerEnterEventTag = delegate { };
	public event EventHandlers.MinimalHandler TriggerExitEventTag = delegate { };
	public event EventHandlers.MinimalHandler TriggerStayEventTag = delegate { };
	
	void OnTriggerEnter(Collider otherOther)
	{
		if(otherOther.tag == tagged)
		{
			TriggerEnterEventTag();
		}
	}
	
	void OnTriggerExit(Collider otherOther)
	{
		if(otherOther.tag == tagged)
		{
			TriggerExitEventTag();
		}
	}
	
	void OnTriggerStay(Collider otherOther)
	{
		if(otherOther.tag == tagged)
		{
			TriggerStayEventTag();
		}
	}
}
