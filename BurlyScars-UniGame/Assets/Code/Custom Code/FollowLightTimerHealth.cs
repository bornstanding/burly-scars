﻿using UnityEngine;
using System.Collections;

public class FollowLightTimerHealth : MonoBehaviour
{
    public CheckLightLevel checkLightLevel;
    public OSUpdateTimer updateTimer;

    void Update()
    {
        updateTimer.timerRate = 3 - (checkLightLevel.lightLevel/5);
    }
}
