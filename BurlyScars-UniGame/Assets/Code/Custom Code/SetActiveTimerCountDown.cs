using UnityEngine;
using System.Collections;

public class SetActiveTimerCountDown : MonoBehaviour 
{
	public TimerCountDown timerCountDown;
	public float lessThen;
	public GameObject thingToTurnOn;
	public bool offOrOn;
	
	void Update()
	{
		if(timerCountDown.timer <= lessThen)
		{
			thingToTurnOn.SetActive(offOrOn);
		}
	}
}
