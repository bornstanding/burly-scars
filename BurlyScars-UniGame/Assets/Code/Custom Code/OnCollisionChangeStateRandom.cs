using UnityEngine;
using System.Collections;

public class OnCollisionChangeStateRandom : MonoBehaviour
{
	public StateManager currentState;
	public StateManager nextState1;
	public StateManager nextState2;
	public string tagged;
	public int randomNumber;
	
	void OnCollisionEnter(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			randomNumber = Random.Range(1,10);
			if(randomNumber <= 5)
			{
				currentState.ChangeState(nextState1);
			}
			
			else
			{
				currentState.ChangeState(nextState2);
			}
		}
	}
}
