using UnityEngine;
using System.Collections;

public class FunctionChangeBullet : MonoBehaviour 
{
	public OnStateEnterChangeBullet bulletChange;
	public Rigidbody emptyBullet;
	
	public void ChangeBullet()
	{
		bulletChange.changeBullet = emptyBullet;
	}
}
