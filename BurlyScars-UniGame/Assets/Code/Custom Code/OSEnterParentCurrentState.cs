using UnityEngine;
using System.Collections;

public class OSEnterParentCurrentState : StateBase 
{
	public ParentChangeToOneState currentParentState;
	public StateManager currentState;
	
	public override void OnStateEnter(Object sender)
	{
		currentParentState.currentState = currentState;
	}
}
