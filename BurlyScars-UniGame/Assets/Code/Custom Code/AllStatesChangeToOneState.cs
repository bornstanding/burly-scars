using UnityEngine;
using System.Collections;

public class AllStatesChangeToOneState : MonoBehaviour 
{
	public StateManager stateManger1;
	public StateManager stateManger2;
	public StateManager stateManger3;
	public StateManager stateManger4;
	public StateManager changeToThisState;
	public string tagged;
	
	void OnCollisionEnter(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			stateManger1.ChangeState(changeToThisState);
			stateManger2.ChangeState(changeToThisState);
			stateManger3.ChangeState(changeToThisState);
			stateManger4.ChangeState(changeToThisState);
		}
	}
}
