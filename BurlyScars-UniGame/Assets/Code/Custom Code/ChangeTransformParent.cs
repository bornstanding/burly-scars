using UnityEngine;
using System.Collections;

public class ChangeTransformParent : StateBase
{
	public OSEnterTransformParent transformParent;
	public GameObject newThingToChild;
	
	public override void OnStateEnter(Object sender)
	{
		transformParent.thingToChild = newThingToChild;
	}
}
