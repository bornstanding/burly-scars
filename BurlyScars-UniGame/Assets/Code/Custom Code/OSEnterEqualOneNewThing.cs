using UnityEngine;
using System.Collections;

public class OSEnterEqualOneNewThing : StateBase
{
	public ChangeBulletMulti changeBulletMulti;
	public Rigidbody newThing;
	
	public override void OnStateEnter(Object sender)
	{
		changeBulletMulti.newThingToSpawn = newThing;
	}
}
