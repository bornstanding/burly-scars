using UnityEngine;
using System.Collections;

public class ChangeStateTo4States : MonoBehaviour 
{
	public StateManager currentState;
	public StateManager firstState;
	public StateManager secondState;
	public StateManager thirdState;
	public StateManager forthState;
	
	public void GoToFirstState()
	{
		currentState.ChangeState(firstState);
	}
	
	public void GoToSecondState()
	{
		currentState.ChangeState(secondState);
	}
	
	public void GoToThirdState()
	{
		currentState.ChangeState(thirdState);
	}
	
	public void GoToForthState()
	{
		currentState.ChangeState(forthState);
	}
}
