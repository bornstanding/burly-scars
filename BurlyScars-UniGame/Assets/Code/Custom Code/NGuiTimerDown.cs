using UnityEngine;
using System.Collections;

public class NGuiTimerDown : MonoBehaviour 
{
	public TimerCountDown timerCountDown;
	public UILabel uILabel;
	public string labelInfo;
	
	void Update()
	{
		uILabel.text = labelInfo + timerCountDown.timer.ToString("f1");
	}
}
