using UnityEngine;
using System.Collections;

public class ChangeBulletMulti : MonoBehaviour 
{
	public AddVelocityForward[] addVelocityForward;
	public Rigidbody newThingToSpawn;
	
	void Update()
	{
		foreach(AddVelocityForward item in addVelocityForward)
			item.thingToSpawn = newThingToSpawn;
	}
}
