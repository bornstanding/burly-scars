﻿using UnityEngine;
using System.Collections;

public class CountTrigger : MonoBehaviour 
{
	public bool addTo;

	void OnTriggerEnter(Collider otherThing)
	{
		Count otherCount = otherThing.gameObject.GetComponent<Count>();
		if (otherCount != null)
		{
			if(addTo)
			{
				otherCount.AddToCount();
			}

			else
			{
				otherCount.MinusToCount();
			}
		}
		return;
	}
}
