﻿using UnityEngine;
using System.Collections;

public class CountTriggerReaderEvent : MonoBehaviour 
{
	public event EventHandlers.MinimalHandler EqualAmountEvent = delegate { };

	public float equalAmount;
	public bool destroy;

	void OnTriggerEnter(Collider otherThing)
	{
		Count otherCount = otherThing.gameObject.GetComponent<Count>();
		if (otherCount != null)
		{
			if(otherCount.num == equalAmount)
			{
				EqualAmountEvent();
				if(destroy)
				{
					Destroy(gameObject);
				}
			}
		}
		return;
	} 

}
