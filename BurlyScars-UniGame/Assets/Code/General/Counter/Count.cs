﻿using UnityEngine;
using System.Collections;

public class Count : MonoBehaviour 
{
	public float num;
	public float addAmount;
	public float minusAmount;

	public void AddToCount()
	{
		num += addAmount;
	}

	public void MinusToCount()
	{
		num -= minusAmount;
	}
}
