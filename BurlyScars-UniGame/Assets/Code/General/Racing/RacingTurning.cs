﻿using UnityEngine;
using System.Collections;

public class RacingTurning : MonoBehaviour
{
    public GameObject mainBody;
    public float currentTurning;
    public KeyCode leftTurn;
    public KeyCode rightTurn;

    private void Update()
    {
        if (Input.GetKey(leftTurn))
        {
            transform.Rotate(-Vector3.up*Time.deltaTime * currentTurning);
        }

        if (Input.GetKey(rightTurn))
        {
            transform.Rotate(Vector3.up * Time.deltaTime * currentTurning);
        }
    }
}
