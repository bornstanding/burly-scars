﻿using UnityEngine;
using System.Collections;

public class RacingMod : MonoBehaviour
{
    public GameObject mainObject;
    public float currentHealth;
    public float maxHealth;
    public float healingHealthRate;
    public float losingHealthRate;
    public float currentSpeed;
    public float startMaxSpeed;
    public float boostMaxSpeed;
    public float maxSpeed;
    public float minSpeed;
    public float acceleration;
    public float boostAcceleration;
    public float startAcceleration;
    public float deacceleration;
    public float brakeSpeed;
    public float frictionSpeed;
    public float turningSpeed;
    public float rotateSpeed;
    public float loseSpeedByDrift;
    public float driftingSpeed;
    public float driftForce;
    public KeyCode leftTurn;
    public KeyCode rightTurn;
    public KeyCode leftDrift;
    public KeyCode rightDrift;
    public KeyCode accelerateButton;
    public KeyCode brakeButton;
    public KeyCode boostButton;

    private void Awake()
    {
        currentHealth = maxHealth;
        acceleration = startAcceleration;
    }

    void Update()
     {
        mainObject.transform.Translate(Vector3.forward * Time.deltaTime * currentSpeed);

        if (!Input.GetKey(boostButton))
        {
            currentHealth += healingHealthRate*Time.deltaTime;
        }

        if (Input.GetKey(accelerateButton) && !Input.GetKey(leftDrift) && !Input.GetKey(rightDrift))
         {
             currentSpeed += acceleration*Time.deltaTime;
         }

         else
         {
             if (currentSpeed >= 0)
             {
                 currentSpeed -= frictionSpeed*Time.deltaTime;
             }
         }

         if (Input.GetKey(brakeButton))
         {
             currentSpeed -= brakeSpeed * Time.deltaTime;
         }

         if (currentSpeed >= maxSpeed)
         {
             currentSpeed = maxSpeed;
         }

         if (currentSpeed <= minSpeed)
         {
             currentSpeed = minSpeed;
         }

        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            maxSpeed = 0;
            boostMaxSpeed = 0;
            startMaxSpeed = 0;
            driftingSpeed = 0;
        }

        if (currentSpeed <= 0 && !Input.GetKey(brakeButton))
         {
             currentSpeed += frictionSpeed * Time.deltaTime;
         }

         if (Input.GetKey(leftTurn))
         {
             transform.Rotate(-Vector3.up * Time.deltaTime * turningSpeed);
         }

         if (Input.GetKey(rightTurn))
         {
             transform.Rotate(Vector3.up * Time.deltaTime * turningSpeed);
         }

         if (Input.GetKey(leftDrift))
         {
             transform.Rotate(-Vector3.up * Time.deltaTime * rotateSpeed);
             currentSpeed -= loseSpeedByDrift * Time.deltaTime;
             mainObject.rigidbody.AddForce(transform.TransformDirection(Vector3.right) * driftForce);
             mainObject.rigidbody.AddForce(transform.TransformDirection(Vector3.forward) * driftForce);
             if (currentSpeed <= driftingSpeed)
             {
                 currentSpeed = driftingSpeed;
             }
         }

         if (Input.GetKey(rightDrift))
         {
             transform.Rotate(Vector3.up * Time.deltaTime * rotateSpeed);
             currentSpeed -= loseSpeedByDrift * Time.deltaTime;
             mainObject.rigidbody.AddForce(transform.TransformDirection(Vector3.left) * driftForce);
             mainObject.rigidbody.AddForce(transform.TransformDirection(Vector3.forward) * driftForce);
             if (currentSpeed <= driftingSpeed)
             {
                 currentSpeed = driftingSpeed;
             }
         }

         if (Input.GetKey(boostButton))
         {
             //StartCoroutine(Boost());
             acceleration = boostAcceleration;
             maxSpeed = boostMaxSpeed;
             currentHealth -= losingHealthRate * Time.deltaTime;
         }

         if (!Input.GetKey(boostButton))
         {
             acceleration = startAcceleration;
             maxSpeed = Mathf.MoveTowards(maxSpeed, startMaxSpeed, deacceleration * Time.deltaTime);
         }
     }

    /*
    IEnumerator Boost()
    {
        acceleration = boostAcceleration;
        maxSpeed = boostMaxSpeed;
        yield return new WaitForSeconds(2);
        acceleration = startAcceleration;
        maxSpeed = startMaxSpeed;
    }
     */
}
