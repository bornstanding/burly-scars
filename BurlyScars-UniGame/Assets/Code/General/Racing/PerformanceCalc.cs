﻿using UnityEngine;
using System.Collections;

public class PerformanceCalc : MonoBehaviour
{
    public RacingMod racingMod;
    public float speedRank;
    public float turnRank;
    public float healRank;
    public float boostRank;
    public float total;
    public float totalMax;

    private void Awake()
    {
        //Speed
        racingMod.startMaxSpeed = (speedRank*5) + 40;
        racingMod.loseSpeedByDrift = speedRank*1.2f;
        racingMod.maxSpeed = racingMod.startMaxSpeed;

        //Turning
        //racingMod.turningSpeed = ((turnRank*12) + 10) + ((racingMod.maxSpeed - racingMod.startMaxSpeed)/2);
        //racingMod.rotateSpeed = racingMod.turningSpeed*2;

        //Speed and Turning
        racingMod.driftingSpeed = (speedRank + turnRank) + 20;
        racingMod.driftForce = ((turnRank * 2) + 35);

        //Heal
        racingMod.healingHealthRate = healRank * 0.5f;

        //Boost
        racingMod.startAcceleration = (boostRank*1.5f) + 10;
        racingMod.boostAcceleration = boostRank*10;
        racingMod.boostMaxSpeed = Mathf.Round((boostRank * 8) + ((9000/(speedRank + 10))/racingMod.startMaxSpeed)  + racingMod.startMaxSpeed);
        racingMod.deacceleration = Mathf.Round((40/boostRank) + 80);

        total = speedRank + turnRank + healRank + boostRank;

        if (totalMax >= totalMax+1)
        {
            total = totalMax;
        }
    }

    private void Update()
    {
        //Turning
        racingMod.turningSpeed = ((turnRank * 8) + 40) + ((racingMod.maxSpeed - racingMod.startMaxSpeed) / 4);
        racingMod.rotateSpeed = racingMod.turningSpeed * 2;
    }
}
