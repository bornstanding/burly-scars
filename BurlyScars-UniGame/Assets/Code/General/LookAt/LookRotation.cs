﻿using UnityEngine;
using System.Collections;

public class LookRotation : MonoBehaviour
{
    public GameObject lookAtObject;
    public GameObject mainObject;
    public float rotationAmount;
    public string tagged;
    public bool lookAtPlayer;

    void Awake()
    {
        if (lookAtPlayer)
        {
            rotationAmount = 90;
        }

        else
        {
            rotationAmount = 270;
        }
    }

    void Update()
    {
		lookAtObject = GameObject.FindGameObjectWithTag(tagged);

		if(lookAtObject != null)
		{
	        var newRotation = Quaternion.LookRotation(lookAtObject.transform.position - mainObject.transform.position).eulerAngles;
	        newRotation.x = 0;
	        newRotation.z = 0;
	        mainObject.transform.rotation = Quaternion.Euler(0, newRotation.y - rotationAmount, 0);
		}

		else
		{
			return;
		}

        // 90 = at Player
        // 270 = away from player

        /*Vector3 targetPosition = (lookAtObject.transform.position - transform.position).normalized;
        Quaternion rotation = Quaternion.LookRotation(targetPosition);
        transform.rotation = rotation;*/
    }
}
