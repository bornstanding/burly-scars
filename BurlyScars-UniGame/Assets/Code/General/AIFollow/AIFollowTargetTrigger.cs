﻿using UnityEngine;
using System.Collections;

public class AIFollowTargetTrigger : MonoBehaviour 
{
	public AIFollow aiFollow;
	public string tagged;
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			aiFollow.target = otherThing.transform;
		}
	}
}
