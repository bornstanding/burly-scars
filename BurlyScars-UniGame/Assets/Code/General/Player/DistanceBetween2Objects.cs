﻿using UnityEngine;
using System.Collections;

public class DistanceBetween2Objects : MonoBehaviour 
{
	public GameObject character;
	public GameObject follower;
	public float distanceBetween;
	public string tagged;
	
	void Update()
	{
		character = GameObject.FindWithTag(tagged);

		if(character != null)
		{
			distanceBetween = Vector3.Distance(character.transform.position, follower.transform.position);
		}

		else
		{
			return;
		}
	}
}
