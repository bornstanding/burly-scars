﻿using UnityEngine;
using System.Collections;

public class BoostingType : MonoBehaviour
{
    public ReactionZone triggerLerp;
    public BoostCalculation boostCalculation;
    public float boostType;

    private void Update()
    {
        if (triggerLerp.score >= boostCalculation.minOK && triggerLerp.score <= boostCalculation.minPerfect)
        {
            boostType = 1;
        }

        if (triggerLerp.score >= boostCalculation.minPerfect && triggerLerp.score <= boostCalculation.maxPerfect)
        {
            boostType = 2;
        }

        if (triggerLerp.score >= boostCalculation.maxPerfect && triggerLerp.score <= boostCalculation.maxOK)
        {
            boostType = 1;
        }

        if(triggerLerp.score <= boostCalculation.minOK && triggerLerp.score >= boostCalculation.maxOK)
        {
            boostType = 0;
        }
    }

    private void OnTriggerStay(Collider otherThing)
    {
        Runner otherBoost = otherThing.GetComponent<Runner>();
        if (otherBoost != null)
        {
            otherBoost.boostType = boostType;
            Debug.Log("boost type" + boostType);
        }

        else
        {
            return;
        }
    }
}
