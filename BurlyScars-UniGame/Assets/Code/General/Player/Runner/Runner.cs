﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody))]
public class Runner : MonoBehaviour, IRunner
{
    #region serialized floats
    [SerializeField] 
    private float _speed;
    [SerializeField] 
    private float _reactionTimer;
    [SerializeField] 
    private float _boostAmount;
    [SerializeField] 
    private bool _drifting;
    [SerializeField] 
    private float _currentSpeed;
    [SerializeField] 
    private float _driftSpeed;
    [SerializeField]
    private bool _boosting;
    #endregion
    private Rigidbody _rigidbody;
    public float boostTimer;
    public float boostType;

    #region Floats

    public float Speed
    {
        get { return _speed; }
        set { _speed = value; }
    }

    public float ReactionTimer
    {
        get { return _reactionTimer; }
        set { _reactionTimer = value; }
    }

    public float BoostAmount
    {
        get { return _boostAmount; }
        set { _boostAmount = value; }
    }

    public bool Drifting
    {
        get { return _drifting; }
        set { _drifting = value; }
    }

    public bool Boosting
    {
        get { return _boosting; }
        set { _boosting = value; }
    }

    public float CurrentSpeed
    {
        get { return _currentSpeed; }
        set { _currentSpeed = value; }
    }

    public float DriftSpeed
    {
        get { return _driftSpeed; }
        set { _driftSpeed = value; }
    }
    #endregion

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _drifting = false;
        _currentSpeed = _speed;
    }

    private void Update()
    {
        Move();
        if (_drifting)
        {
            _currentSpeed = _driftSpeed;
        }
        else
        {
            _currentSpeed = _speed;
        }
        if (_boosting)
        {
            StartCoroutine(BoostCoroutine(boostTimer));
        }
    }
    public void Move()
    {
        _rigidbody.velocity = transform.TransformDirection(Vector3.forward*(_currentSpeed + 5));
    }
    public void Boost()
    {
        if (boostType != 0)
            _boosting = true;
    }
    private IEnumerator BoostCoroutine(float value)
    {
        _currentSpeed = ((_boostAmount + 15) * boostType);
        yield return new WaitForSeconds(value);
        boostType = 0;
        _currentSpeed = _speed;
        _boosting = false;
    }

}
  
