﻿using UnityEngine;
using System.Collections;

public class IsDrifting : MonoBehaviour
{
    public float yRotation;

    private ReactionZone _triggerLerp;

    void Awake()
    {
        _triggerLerp = gameObject.GetComponent<ReactionZone>();
    }

    void OnTriggerEnter(Collider otherThing)
    {
        Runner runner = otherThing.GetComponent<Runner>();
        if (runner != false)
        {
            runner.Drifting = true;
        }
    }
    void OnTriggerExit(Collider otherThing)
    {
        Runner runner = otherThing.GetComponent<Runner>();
        if (runner != false)
        {
            runner.Drifting = false;
            Quaternion newRotation = runner.gameObject.transform.rotation;
            newRotation.y = yRotation;
            runner.gameObject.transform.rotation = newRotation;
        }
        _triggerLerp.score = 0;
        if (runner != null)
        {
            runner.Boost();
        }
    }
}
