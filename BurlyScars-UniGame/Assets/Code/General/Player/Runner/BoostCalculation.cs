﻿using UnityEngine;
using System.Collections;

public class BoostCalculation : MonoBehaviour
{
    public float minOK;
    public float maxOK;
    public float minPerfect;
    public float maxPerfect;
    public float okRange;

    void OnTriggerEnter()
    {
        CalculateBoostRange();
    }

    public void CalculateBoostRange()
    {
        minOK = Random.Range(5, 55);
        okRange = Random.Range(30, 40);
        maxOK = minOK + okRange;
        minPerfect = minOK + (int) okRange/3;
        maxPerfect = minOK + (int) ((okRange/3)*2);
    }
}
