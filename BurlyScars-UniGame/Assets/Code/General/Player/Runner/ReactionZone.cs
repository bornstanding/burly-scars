﻿using UnityEngine;
using System.Collections;

public class ReactionZone : MonoBehaviour
{
    public Runner runner;
    public float timer;
    public KeyCode reactionButton;
    public float score;
 
    private void Update()
    {
        if (runner.Drifting)
        {
            timer = Mathf.MoveTowards(timer, 100, 450/runner.ReactionTimer * Time.deltaTime);
            if (Input.GetKeyDown(reactionButton) && score == 0)
            {
                score = timer;
            }
            else
            {
                return;
            }
        }
    }
}
