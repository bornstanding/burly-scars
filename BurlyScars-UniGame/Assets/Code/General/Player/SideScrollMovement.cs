﻿using UnityEngine;
using System.Collections;

public class SideScrollMovement : MonoBehaviour
{
    public Rigidbody mainBody;
    public float moveSpeed;
    public float acceleration;
    public float maxSpeed;
    public float friction;
    public KeyCode moveLeftButton;
    public KeyCode moveRightButton;

   void Update()
   {

            //Current Speed

            //Move Left
            if (Input.GetKey(moveLeftButton))
            {
			    //mainBody.transform.eulerAngles = new Vector3(0, 0, 0);
                moveSpeed += acceleration*Time.deltaTime;

                //mainBody.velocity = (transform.TransformDirection(Vector3.left * moveSpeed));
            }

            //Move Right
            if (Input.GetKey(moveRightButton))
            {
			    //mainBody.transform.eulerAngles = new Vector3(0, 180, 0);
                moveSpeed += acceleration*Time.deltaTime;

                //mainBody.velocity = (transform.TransformDirection(Vector3.left * moveSpeed));
            }

            //Friction
       
            if (!Input.GetKey(moveRightButton) && !Input.GetKey(moveLeftButton))
            {
                moveSpeed -= friction*Time.deltaTime;

                if (moveSpeed <= 0)
                {
                    moveSpeed = 0;
                }
            }
        

            //Max Speed
            if (moveSpeed >= maxSpeed)
            {
                moveSpeed = maxSpeed;
            }

            //No Movement
            if (moveLeftButton == null || moveRightButton == null)
            {
                return;
            }
    }
}
