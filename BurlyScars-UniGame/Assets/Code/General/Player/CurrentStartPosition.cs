using UnityEngine;
using System.Collections;

public class CurrentStartPosition : MonoBehaviour 
{
	public Transform currentPosition;
	public Transform startPosition;
	public Vector3 sPosition;
	public Vector3 cPosition;
	
	void Awake()
	{
		startPosition.position = gameObject.transform.position;
		sPosition = startPosition.position;
	}
	
	void Update()
	{
		currentPosition.position = gameObject.transform.position;
		cPosition = currentPosition.position;
	}
}
