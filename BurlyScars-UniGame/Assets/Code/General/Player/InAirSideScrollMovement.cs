﻿using UnityEngine;
using System.Collections;

public class InAirSideScrollMovement : StateBase
{
    public Rigidbody mainBody;
    public float moveSpeed;
    public float acceleration;
    public float maxSpeed;
	public float maxVelocity;
	public float currentVelocity;
    public float friction;
    public KeyCode moveLeftButton;
    public KeyCode moveRightButton;

    public override void OnStateFixedUpdate(Object sender)
    {
        base.OnStateFixedUpdate(sender);

		currentVelocity = mainBody.rigidbody.velocity.x;

        //Current Speed
        mainBody.AddForce(transform.TransformDirection(Vector3.left * moveSpeed));

        //Move Left
        if (Input.GetKey(moveLeftButton))
        {
            mainBody.transform.eulerAngles = new Vector3(0, 0, 0);
            moveSpeed += acceleration * Time.deltaTime;
        }

        //Move Right
        if (Input.GetKey(moveRightButton))
        {
            mainBody.transform.eulerAngles = new Vector3(0, 180, 0);
            moveSpeed += acceleration * Time.deltaTime;
        }

        //Friction
        if (!Input.GetKey(moveRightButton) && !Input.GetKey(moveLeftButton))
        {
            moveSpeed -= friction * Time.deltaTime;

            if (moveSpeed <= 0)
            {
                moveSpeed = 0;
            }
        }

        //Max Speed
        if (moveSpeed >= maxSpeed)
        {
            moveSpeed = maxSpeed;
        }

        //Min Speed
        if (moveSpeed <= -maxSpeed)
        {
            moveSpeed = -maxSpeed;
        }

		if (currentVelocity >= maxVelocity)
		{
			currentVelocity = maxVelocity;
		}

		if (currentVelocity <= -maxVelocity)
		{
			currentVelocity = -maxVelocity;
		}

        //No Movement
        if (moveLeftButton == null || moveRightButton == null)
        {
            return;
        }
    }

    public override void OnStateExit(Object sender)
    {
        base.OnStateExit(sender);

        moveSpeed = 0;
    }
}
