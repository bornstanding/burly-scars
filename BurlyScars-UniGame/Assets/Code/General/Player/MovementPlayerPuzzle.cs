﻿using UnityEngine;
using System.Collections;

public class MovementPlayerPuzzle : MonoBehaviour
{
    public float moveSpeed;
    public GameObject mainGameObject;
    public KeyCode upButton;
    public KeyCode downButton;
    public KeyCode rightButton;
    public KeyCode leftButton;

    private void Update()
    {
        if (Input.GetKeyDown(upButton))
        {
            mainGameObject.transform.position += Vector3.forward * moveSpeed;
        }

        if (Input.GetKeyDown(downButton))
        {
            mainGameObject.transform.position += Vector3.back * moveSpeed;
        }

        if (Input.GetKeyDown(rightButton))
        {
            mainGameObject.transform.position += Vector3.right * moveSpeed;
        }

        if (Input.GetKeyDown(leftButton))
        {
            mainGameObject.transform.position += Vector3.left*moveSpeed;
        }
    }
}
