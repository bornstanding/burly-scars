﻿using UnityEngine;
using System.Collections;

public class MovementRotatePlayerPuzzle : MonoBehaviour
{
    public GameObject mainGameObject;
    public KeyCode upRotation;
    public KeyCode downRotation;
    public KeyCode rightRotation;
    public KeyCode leftRotation;

    private void Update()
    {
        if (Input.GetKeyDown(upRotation))
        {
            mainGameObject.transform.eulerAngles = new Vector3(0, 0, 0);
        }

        if (Input.GetKeyDown(downRotation))
        {
            mainGameObject.transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if (Input.GetKeyDown(rightRotation))
        {
            mainGameObject.transform.eulerAngles = new Vector3(0, 90, 0);
        }

        if (Input.GetKeyDown(leftRotation))
        {
            mainGameObject.transform.eulerAngles = new Vector3(0, 270, 0);
        }
    }
}
