﻿using UnityEngine;
using System.Collections;

public class TwoDPlayerMovement : MonoBehaviour 
{
	public Rigidbody mainBody;
	public float verticalSpeed;
	public KeyCode upButton;
	public KeyCode downButton;
	
	void Update()
	{
		if(Input.GetKey(upButton))
		{
		    mainBody.velocity = transform.TransformDirection(Vector3.up * verticalSpeed);
		}

        if (upButton == null)
        {
            return;
        }
		
		if(Input.GetKey(downButton))
		{
		    mainBody.velocity = transform.TransformDirection(Vector3.down * verticalSpeed);
		}

        if (downButton == null)
        {
            return;
        }
	}
}
