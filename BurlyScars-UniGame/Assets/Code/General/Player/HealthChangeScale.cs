﻿using UnityEngine;
using System.Collections;

public class HealthChangeScale : MonoBehaviour
{
    public StandardHealth standardHelth;
    public GameObject changeScaleObject;
    public Vector3 sizeMultiplier;
    public Vector3 miniumScale;
    public Vector3 zeroHealthScale;
    public float lerpTimer;

    void Update()
    {
        changeScaleObject.transform.localScale = (sizeMultiplier*standardHelth.currentHealth) + miniumScale;
        if (standardHelth.currentHealth <= 0)
        {
            miniumScale = Vector3.MoveTowards(miniumScale, zeroHealthScale, lerpTimer*Time.deltaTime);
        }
    }
}
