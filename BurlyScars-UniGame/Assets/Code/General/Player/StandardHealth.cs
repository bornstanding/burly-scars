using UnityEngine;
using System.Collections;

public class StandardHealth : MonoBehaviour 
{
	public float currentHealth;
	public float maxHealth;
	
	public event EventHandlers.MinimalHandler DeathEvent = delegate { };

	void Awake()
	{
		currentHealth = maxHealth;
	}
	
	void Update()
	{
		if(currentHealth >= maxHealth)
		{
			currentHealth = maxHealth;
		}
		
		if(currentHealth <= 0)
		{
			currentHealth = 0;
			DeathEvent();
		}
	}
}
