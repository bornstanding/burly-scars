﻿using UnityEngine;
using System.Collections;

public class DistanceChangeStateMax : MonoBehaviour 
{
	public DistanceBetween2Objects distanceBetweenTwoObjects;
	public StateManager currentState;
	public StateManager nextState;
	public float maxDistance;
	
	void Update()
	{
		if(distanceBetweenTwoObjects.distanceBetween >= maxDistance)
		{
			currentState.ChangeState(nextState);
		}
	}
}
