﻿using UnityEngine;
using System.Collections;

public class ChangeStateHealth : MonoBehaviour
{
    public StandardHealth standardHealth;
    public float healthChangeState;
    public StateManager currentState;
    public StateManager nextState;

    private void Update()
    {
        if (standardHealth.currentHealth == healthChangeState)
        {
            currentState.ChangeState(nextState);
        }
    }
}
