﻿using UnityEngine;
using System.Collections;

public class AITargetNullChangeState : MonoBehaviour 
{
	public AIFollow aiFollow;
	public GameObject newTarget;
	public string tagged;
	
	void Awake()
	{
		newTarget = GameObject.FindWithTag(tagged);
	}
	
	void Update()
	{
		if(aiFollow.target == null)
		{
			aiFollow.target = newTarget.transform;
		}
	}
}
