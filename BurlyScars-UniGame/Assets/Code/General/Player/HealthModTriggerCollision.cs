using UnityEngine;
using System.Collections;

public class HealthModTriggerCollision : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public float modHealth;
	
	void OnCollisionEnter(Collision otherThing)
	{
		StandardHealth otherHealth = otherThing.gameObject.GetComponent<StandardHealth>();
		if(otherHealth != null)
		{
			otherHealth.currentHealth += modHealth;
			Destroy(gameObject);
		}
		else
		{
            Destroy(gameObject);
			return;
		}
		
	}
	
	void OnTriggerEnter(Collider otherThing)
	{
		StandardHealth otherHealth = otherThing.gameObject.GetComponent<StandardHealth>();
		if(otherHealth != null)
		{
			otherHealth.currentHealth += modHealth;
			Destroy(gameObject);
		}
		else
		{
            //Destroy(gameObject);
			return;
		}
		
	}
}
		