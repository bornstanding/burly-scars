﻿using UnityEngine;
using System.Collections;

public class DistanceChangeStateMin : MonoBehaviour 
{
	public DistanceBetween2Objects distanceBetweenTwoObjects;
	public StateManager currentState;
	public StateManager nextState;
	public float minDistance;
	
	void Update()
	{
		if(distanceBetweenTwoObjects.distanceBetween <= minDistance)
		{
			currentState.ChangeState(nextState);
		}
	}
}
