using UnityEngine;
using System.Collections;

public class MovementPlayer : MonoBehaviour
{
	public float moveSpeed;
	public float strafeSpeed;
	public float rotateSpeed;
	public Rigidbody parentBody;
	
	void Update()
	{
		float move = Input.GetAxis("Vertical") * moveSpeed;
		float strafe = Input.GetAxis("Horizontal") * strafeSpeed;
		float rotate = Input.GetAxis("Rotate") * rotateSpeed;
		
		Vector3 moveChar = new Vector3(strafe,(parentBody.rotation * parentBody.velocity).y,move);
		Vector3 rotateCharacter = new Vector3(0,rotate,0);
		
		parentBody.velocity = parentBody.rotation * moveChar;
		parentBody.angularVelocity = transform.rotation * rotateCharacter;
	}
}
