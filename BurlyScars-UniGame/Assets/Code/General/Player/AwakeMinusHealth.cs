﻿using UnityEngine;
using System.Collections;

public class AwakeMinusHealth : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public float healthAmount;
	
	void Awake()
	{
		standardHealth.currentHealth += healthAmount;
	}
}
