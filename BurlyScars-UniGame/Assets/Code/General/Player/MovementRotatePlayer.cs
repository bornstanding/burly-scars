using UnityEngine;
using System.Collections;

public class MovementRotatePlayer : MonoBehaviour 
{
	public float rotateSpeed;
	public Rigidbody parentBody;
	
	void Update()
	{
		float rotate = Input.GetAxis("Rotate") * rotateSpeed;
		
		Vector3 moveChar = new Vector3(0,(parentBody.rotation * parentBody.velocity).y,0);
		Vector3 rotateCharacter = new Vector3(0,rotate,0);
		
		parentBody.velocity = parentBody.rotation * moveChar;
		parentBody.angularVelocity = transform.rotation * rotateCharacter;
	}
}

