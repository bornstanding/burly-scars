﻿using UnityEngine;
using System.Collections;

public class CollisionTriggerTagHealthMod : MonoBehaviour
{
    public StandardHealth standardHealth;
    public float modHealth;
    public string tagged;

    void OnCollisionEnter(Collision otherThing)
    {
        if (otherThing.gameObject.tag == tagged)
        {
            StandardHealth otherHealth = otherThing.gameObject.GetComponent<StandardHealth>();
            if (otherHealth != null)
            {
                otherHealth.currentHealth += modHealth;
            }

            return;
        }
    }

    void OnTriggerEnter(Collider otherThing)
    {
        if (otherThing.tag == tagged)
        {
            StandardHealth otherHealth = otherThing.gameObject.GetComponent<StandardHealth>();
            if (otherHealth != null)
            {
                otherHealth.currentHealth += modHealth;
            }

            return;
        }
    }
}