using UnityEngine;
using System.Collections;

public class TimedDestroy : MonoBehaviour 
{
	public float timer;
	
	void Update()
	{
		timer -= 1 * Time.deltaTime;
		if(timer <= 0)
		{
			Destroy(gameObject);
		}
	}
}
