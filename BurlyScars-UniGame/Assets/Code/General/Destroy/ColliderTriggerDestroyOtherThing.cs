using UnityEngine;
using System.Collections;

public class ColliderTriggerDestroyOtherThing : MonoBehaviour 
{
	public string tagged;
	
	void OnCollisonEnter(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			Destroy(otherThing.gameObject);
		}
	}
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			Destroy(otherThing.gameObject);
		}
	}
}
