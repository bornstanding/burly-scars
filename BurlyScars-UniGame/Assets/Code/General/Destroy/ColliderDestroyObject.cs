using UnityEngine;
using System.Collections;

public class ColliderDestroyObject : MonoBehaviour 
{
	public string tagged;
	
	void OnCollisionEnter(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			Destroy(gameObject);
		}
	}
}
