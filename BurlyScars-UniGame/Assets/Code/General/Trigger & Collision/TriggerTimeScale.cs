﻿using UnityEngine;
using System.Collections;

public class TriggerTimeScale : MonoBehaviour 
{
	public int levelSpeed;
	public string tagged;

	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			Time.timeScale = levelSpeed;
			print (Time.timeScale);
		}
	}
}
