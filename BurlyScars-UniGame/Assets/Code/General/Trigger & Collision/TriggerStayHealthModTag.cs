using UnityEngine;
using System.Collections;

public class TriggerStayHealthModTag : MonoBehaviour 
{
	public string tagged;
	public float modHealth;
	
	void OnTriggerStay(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			StandardHealth otherHealth = otherThing.gameObject.GetComponent<StandardHealth>();
		    if(otherHealth != null)
		    {
			    otherHealth.currentHealth += modHealth * Time.deltaTime;
		    }
		
		return;
		}
	}
}
