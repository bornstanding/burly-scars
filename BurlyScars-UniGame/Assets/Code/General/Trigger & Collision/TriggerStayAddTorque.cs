﻿using UnityEngine;
using System.Collections;

public class TriggerStayAddTorque : MonoBehaviour 
{
	public float torqueSpeed;
	public Rigidbody mainBody;
	public string tagged;
	
	void OnTriggerStay (Collider otherThing)
	{
		if(otherThing.tag == tagged)	
	    {
		    mainBody.AddTorque(Vector3.up * torqueSpeed);
	    }
	}
}
