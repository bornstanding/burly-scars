using UnityEngine;
using System.Collections;

public class TriggerEnterChangeState : MonoBehaviour 
{
	public string tagged;
	public StateManager currentState;
	public StateManager nextState;
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			currentState.ChangeState(nextState);
		}
	}
}
