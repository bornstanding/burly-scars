using UnityEngine;
using System.Collections;

public class TriggerActivate : MonoBehaviour 
{
	public IsActivated isActivated;
	public string tagged;
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
		    isActivated.isActivated = true;
		}
	}
	
	void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
		    isActivated.isActivated = false;
		}
	}
}
