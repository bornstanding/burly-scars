﻿using UnityEngine;
using System.Collections;

public class TriggerAddVelocity : MonoBehaviour
{
    public Rigidbody mainBody;
    public Vector3 velocityAmount;
    public string tagged;

    void OnCollisionEnter(Collision otherThing)
    {
        if (otherThing.gameObject.tag == tagged)
        {
            Rigidbody otherBody = otherThing.gameObject.GetComponent<Rigidbody>();
            if (otherBody != null)
            {
                otherBody.velocity = velocityAmount;
            }

            return;
        }
    }

    void OnTriggerEnter(Collider otherThing)
    {
        if (otherThing.tag == tagged)
        {
            Rigidbody otherBody = otherThing.gameObject.GetComponent<Rigidbody>();
            if (otherBody != null)
            {
                otherBody.velocity = velocityAmount;
            }

            return;
        }
    }
}
