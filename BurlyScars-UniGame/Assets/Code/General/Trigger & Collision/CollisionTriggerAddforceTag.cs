﻿using UnityEngine;
using System.Collections;

public class CollisionTriggerAddforceTag : MonoBehaviour
{
    public Rigidbody mainBody;
    public Vector3 forceAmount;
	public string tagged;
	
	void OnCollisionEnter(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			Rigidbody otherBody = otherThing.gameObject.GetComponent<Rigidbody>();
		    if(otherBody != null)
		    { 
			    otherBody.AddForce(forceAmount);
		    }
		
		return;
		}
	}
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			Rigidbody otherBody = otherThing.gameObject.GetComponent<Rigidbody>();
		    if(otherBody != null)
		    {
			    otherBody.AddForce(forceAmount);
		    }
		
		return;
		}
	}
}

