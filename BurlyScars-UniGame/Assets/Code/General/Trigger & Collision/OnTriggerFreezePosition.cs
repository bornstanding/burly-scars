﻿using UnityEngine;
using System.Collections;

public class OnTriggerFreezePosition : MonoBehaviour
{
    public bool xFreezePosition;
    public bool yFreezePosition;
    public bool zFreezePosition;

    void OnTriggerEnter(Collider otherThing)
    {
        Rigidbody otherRigidBody = otherThing.GetComponent<Rigidbody>();
        if (otherRigidBody != null)
        {
            if (xFreezePosition)
            {
                otherRigidBody.constraints &= ~RigidbodyConstraints.FreezePositionX;
            }

            if (yFreezePosition)
            {
                otherRigidBody.constraints &= ~RigidbodyConstraints.FreezePositionY;
            }

            if (zFreezePosition)
            {
                otherRigidBody.constraints &= ~RigidbodyConstraints.FreezePositionZ;
            }
        }
    }
}
