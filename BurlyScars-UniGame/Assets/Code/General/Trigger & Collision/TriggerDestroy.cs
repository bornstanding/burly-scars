using UnityEngine;
using System.Collections;

public class TriggerDestroy : MonoBehaviour 
{
	public string tagged;
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			Destroy(gameObject);
		}
	}
}