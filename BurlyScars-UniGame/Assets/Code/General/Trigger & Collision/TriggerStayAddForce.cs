using UnityEngine;
using System.Collections;

public class TriggerStayAddForce : MonoBehaviour 
{
	public GameObject mainPosition;
	public GameObject addForceToObject;
	public Vector3 forceAmount;
	public string tagged;
	public float forceMulti;
	public float xForceMulti = 1;
	public float yForceMulti = 1;
	public float zForceMulti = 1;
	
	void OnTriggerStay(Collider otherThing)
	{
		addForceToObject = GameObject.FindWithTag(tagged);

		if(otherThing.tag == tagged)
		{
			forceAmount = (addForceToObject.transform.position - mainPosition.transform.position).normalized * forceMulti;
			addForceToObject.rigidbody.AddForce((forceAmount.x * xForceMulti), (forceAmount.y * yForceMulti), (forceAmount.z * zForceMulti));
		}
	}
}
