﻿using UnityEngine;
using System.Collections;

public class TriggerTagInstantiate : MonoBehaviour
{
    public string tagged;
    public Rigidbody thingToSpawn;
    public GameObject spawnPoint;

    private void OnTriggerEnter(Collider otherThing)
    {
        if (otherThing.tag == tagged)
        {
            Rigidbody clone;
            clone = Instantiate(thingToSpawn, spawnPoint.transform.position, spawnPoint.transform.rotation) as Rigidbody;
        }
    }
}
