using UnityEngine;
using System.Collections;

public class TriggerChangeVelocity : MonoBehaviour 
{
	public string tagged;
	public Vector3 newVelocityDirection;
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == tagged)
		{
			other.rigidbody.velocity = (newVelocityDirection);
		}
	}
}
