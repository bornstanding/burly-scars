﻿using UnityEngine;
using System.Collections;

public class CheckOnGrassWrong : MonoBehaviour 
{
	public OnGrass onGrass;
	public GameObject mainPosition;
	public Vector3 forceAmount;
	public float forceMulti;
	public float healthMod;
	
	void OnTriggerStay(Collider otherThing)
	{
		OnGrass otherGrass = otherThing.gameObject.GetComponent<OnGrass>();
		if(otherGrass != null)
		{
			forceAmount = (otherThing.transform.position - mainPosition.transform.position) * forceMulti;
		    otherThing.rigidbody.AddForce(forceAmount);
		}
		
		if(otherGrass == null)
		{
			return;
		}
	}
	
	void OnTriggerEnter(Collider otherThing)
	{	
		StandardHealth otherHealth = otherThing.GetComponent<StandardHealth>();
		OnGrass otherGrass = otherThing.gameObject.GetComponent<OnGrass>();
		if(otherHealth != null && otherGrass != null)
		{
			otherHealth.currentHealth += healthMod;
		}
		
		return;
	}
}
