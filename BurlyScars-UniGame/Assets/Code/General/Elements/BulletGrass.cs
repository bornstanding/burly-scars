using UnityEngine;
using System.Collections;

public class BulletGrass : MonoBehaviour 
{
	public GrassElement grassElement;
	
	void OnTriggerEnter(Collider otherThing)
	{
		GrassElement otherGrass = otherThing.gameObject.GetComponent<GrassElement>();
		if(otherGrass != null)
		{
			otherGrass.isGrass = true;
		}
		
		return;
	}
}
