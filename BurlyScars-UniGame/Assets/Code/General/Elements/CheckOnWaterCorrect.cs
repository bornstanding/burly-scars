using UnityEngine;
using System.Collections;

public class CheckOnWaterCorrect : MonoBehaviour 
{
	public OnWater onWater;
	public StandardHealth standardHealth;
	public float healthMod;
	
	#region Check for component
	void OnTriggerEnter(Collider otherThing)
	/*{	
		IdentityBase otherIdentity = ToolBox.GetComponentInParents<IdentityBase>(otherThing.gameObject.transform);
		if(otherIdentity == null)
		{
			return;
		}
		
		if(otherIdentity != null)
		{
		    OnWater otherWater = otherIdentity.GetComponentInChildren<OnWater>();
		    if(otherWater != null)
		    {
			    standardHealth.currentHealth += healthMod * Time.deltaTime;
		    }
			
			if(otherWater == null)
			{
				return;
			}
		}
	}
	*/
	{
		OnWater otherWater = otherThing.GetComponent<OnWater>();
		if(otherWater != null)
		{
			standardHealth.currentHealth += healthMod ;
			Destroy(otherThing.gameObject);
		}
		
		return;
	}
	#endregion
}
