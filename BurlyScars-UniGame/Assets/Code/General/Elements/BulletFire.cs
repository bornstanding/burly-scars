using UnityEngine;
using System.Collections;

public class BulletFire : MonoBehaviour 
{
	public FireElement fireElement;
	
	void OnTriggerEnter(Collider otherThing)
	{
		FireElement otherFire = otherThing.gameObject.GetComponent<FireElement>();
		if(otherFire != null)
		{
			otherFire.isFire = true;
		}
		
		return;
	}
}
