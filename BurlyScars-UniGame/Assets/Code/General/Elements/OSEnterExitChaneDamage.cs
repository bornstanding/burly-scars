﻿using UnityEngine;
using System.Collections;

public class OSEnterExitChaneDamage : StateBase
{
	public HealthModTriggerCollision healthMod;
	public float normalDamage;
	public float multiDamage;
	
	public override void OnStateEnter (Object sender) 
	{
		healthMod.modHealth = multiDamage;
	}
	
	public override void OnStateExit (Object sender) 
	{
		healthMod.modHealth = normalDamage;
	}
}
