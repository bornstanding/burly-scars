﻿using UnityEngine;
using System.Collections;

public class CheckElementChangeState : MonoBehaviour 
{
	public StateManager fireState;
	public StateManager waterState;
	public StateManager grassState;
	public StateManager currentState;
	
	void OnTriggerEnter(Collider otherThing)
	{
		OnFire otherFire = otherThing.gameObject.GetComponent<OnFire>();
		if(otherFire)
		{
			currentState.ChangeState(fireState);
		}
		
		OnGrass otherGrass = otherThing.gameObject.GetComponent<OnGrass>();
		if(otherGrass)
		{
			currentState.ChangeState(grassState);
		}
		
		OnWater otherWater = otherThing.gameObject.GetComponent<OnWater>();
		if(otherWater)
		{
			currentState.ChangeState(waterState);
		}
	}
}
