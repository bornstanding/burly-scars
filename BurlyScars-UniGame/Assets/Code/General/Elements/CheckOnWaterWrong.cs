using UnityEngine;
using System.Collections;

public class CheckOnWaterWrong : MonoBehaviour 
{
	public OnWater onWater;
	public GameObject mainPosition;
	public Vector3 forceAmount;
	public float forceMulti;
	public float healthMod;
	
	void OnTriggerStay(Collider otherThing)
	{	
	    OnWater otherWater = otherThing.GetComponent<OnWater>();
		if(otherWater != null)
		{
			forceAmount = (otherThing.transform.position - mainPosition.transform.position) * forceMulti;
		    otherThing.rigidbody.AddForce(forceAmount);
		}
		
		return;
	}
	
	void OnTriggerEnter(Collider otherThing)
	{	
		StandardHealth otherHealth = otherThing.GetComponent<StandardHealth>();
		OnWater otherWater = otherThing.GetComponent<OnWater>();
		if(otherHealth != null && otherWater != null)
		{
			otherHealth.currentHealth += healthMod;
		}
		
		return;
	}
}
