using UnityEngine;
using System.Collections;

public class ChangeStateElements : MonoBehaviour 
{
	public FireElement fireElement;
	public WaterElement waterElement;
	public GrassElement grassElement;
	public StateManager fire;
	public StateManager water;
	public StateManager grass;
	public StateManager currentState;
	
	void Update()
	{
		if(fireElement.isFire == true)
		{
			currentState.ChangeState(fire);
		}
		
		if(waterElement.isWater == true)
		{
			currentState.ChangeState(water);
		}
		
		if(grassElement.isGrass == true)
		{
			currentState.ChangeState(grass);
		}
	}
}
