using UnityEngine;
using System.Collections;

public class BulletWater : MonoBehaviour 
{
	public WaterElement waterElement;
	
	void OnTriggerEnter(Collider otherThing)
	{
		WaterElement otherWater = otherThing.gameObject.GetComponent<WaterElement>();
		if(otherWater != null)
		{
			otherWater.isWater = true;
		}
		
		return;
	}
}
