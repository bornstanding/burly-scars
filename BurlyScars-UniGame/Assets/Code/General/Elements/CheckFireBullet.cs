﻿using UnityEngine;
using System.Collections;

public class CheckFireBullet : MonoBehaviour 
{
	public BulletFire bulletFire;
	public StandardHealth standardHealth;
	public float healthMod;
	
	void OnTriggerEnter(Collider otherThing)
	{	
	    BulletFire otherFire = otherThing.GetComponent<BulletFire>();
		if(otherFire != null)
		{
			standardHealth.currentHealth += healthMod;
		}
		
		return;		
	}
}
