using UnityEngine;
using System.Collections;

public class CheckOnGrassCorrect : MonoBehaviour 
{
	public OnGrass onGrass;
	public StandardHealth standardHealth;
	public float healthMod;
	
	void OnTriggerEnter(Collider otherThing)
	{
		OnGrass otherGrass = otherThing.gameObject.GetComponent<OnGrass>();
		if(otherGrass != null)
		{
			standardHealth.currentHealth += healthMod;
			Destroy(otherThing.gameObject);
		}
		
		if(otherGrass == null)
		{
			return;
		}
	}
}
