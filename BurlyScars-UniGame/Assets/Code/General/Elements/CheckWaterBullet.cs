﻿using UnityEngine;
using System.Collections;

public class CheckWaterBullet : MonoBehaviour 
{
	public BulletWater bulletWater;
	public StandardHealth standardHealth;
	public float healthMod;
	
	void OnTriggerEnter(Collider otherThing)
	{	
	    BulletWater otherWater = otherThing.GetComponent<BulletWater>();
		if(otherWater != null)
		{
			standardHealth.currentHealth += healthMod;
		}
		
		return;		
	}
}
