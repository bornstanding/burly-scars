using UnityEngine;
using System.Collections;

public class CheckOnFireWrong : MonoBehaviour 
{
	public OnFire onFire;
	public GameObject mainPosition;
	public Vector3 forceAmount;
	public float forceMulti;
	public float healthMod;
	
	void OnTriggerStay(Collider otherThing)
	{	
		OnFire otherFire = otherThing.GetComponent<OnFire>();
		StandardHealth otherHealth = otherThing.GetComponent<StandardHealth>();
		if(otherFire != null)
		{
			forceAmount = (otherThing.transform.position - mainPosition.transform.position) * forceMulti;
		    otherThing.rigidbody.AddForce(forceAmount);
		}
		
		return;
	}
	
	void OnTriggerEnter(Collider otherThing)
	{	
		StandardHealth otherHealth = otherThing.GetComponent<StandardHealth>();
		OnFire otherFire = otherThing.GetComponent<OnFire>();
		if(otherHealth != null && otherFire != null)
		{
			otherHealth.currentHealth += healthMod;
		}
		
		return;
	}
}
