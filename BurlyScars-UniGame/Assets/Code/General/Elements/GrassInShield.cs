﻿using UnityEngine;
using System.Collections;

public class GrassInShield : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public float healthAmount;
	
	public void GrassEnteredShield()
	{
		standardHealth.currentHealth += healthAmount;
	}
}
