using UnityEngine;
using System.Collections;

public class OSEnterTurnOffElements : StateBase 
{
	public FireElement fireElement;
	public WaterElement waterElement;
	public GrassElement grassElement;
	
	public override void OnStateEnter (Object sender)
	{
		fireElement.isFire = false;
		waterElement.isWater = false;
		grassElement.isGrass = false;
	}
}
