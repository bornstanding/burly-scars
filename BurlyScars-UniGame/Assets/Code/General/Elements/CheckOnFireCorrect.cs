﻿using UnityEngine;
using System.Collections;

public class CheckOnFireCorrect : MonoBehaviour 
{
	public OnFire onFire;
	public StandardHealth standardHealth;
	public float healthMod;
	
	void OnTriggerStay(Collider otherThing)
	{	
		OnFire otherFire = otherThing.GetComponent<OnFire>();
		if(otherFire != null)
		{
			standardHealth.currentHealth += healthMod;
			Destroy(otherThing.gameObject);
		}
		
		return;
	}	
}
