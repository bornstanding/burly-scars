﻿using UnityEngine;
using System.Collections;

public class LoadCurrentLevelButton : MonoBehaviour 
{
	public KeyCode restartButton;

	void Update()
	{
		if(Input.GetKeyDown(restartButton))
		{
			Time.timeScale = 1;
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
