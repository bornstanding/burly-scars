﻿using UnityEngine;
using System.Collections;

public class PauseButtonDown : MonoBehaviour 
{
	public KeyCode pauseButton;
	public KeyCode unpauseButton;

	void Update()
	{
		if(Input.GetKeyDown(pauseButton))
		{
			Time.timeScale = 0;
		}

		if(Input.GetKeyDown(unpauseButton))
		{
			Time.timeScale = 1;
		}
	}
}
