﻿using UnityEngine;
using System.Collections;

public class TeleportRandomPosition : StateBase
{
    public GameObject teleportObject;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    public float zMax;
    public float zMin;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        float xRange = Random.Range(xMin, xMax);
        float yRange = Random.Range(yMin, yMax);
        float zRange = Random.Range(zMin, zMax);
        teleportObject.transform.position = new Vector3(xRange, yRange, zRange);
    }
}
