﻿using UnityEngine;
using System.Collections;

public class FindGameWithTagAwake : MonoBehaviour
{
    public GameObject objectWithTag;
    public string tagged;

	void Update()
	{
		objectWithTag = GameObject.FindGameObjectWithTag(tagged);

		if(objectWithTag == null)
		{
			return;
		}
	}
}
