﻿using UnityEngine;
using System.Collections;

public class FindWithTagAwake : MonoBehaviour 
{
	public CounterEventsThree counterEventThree;
	
	void Awake()
	{
		counterEventThree = GameObject.FindObjectOfType(typeof(CounterEventsThree)) as CounterEventsThree;
	}
}
