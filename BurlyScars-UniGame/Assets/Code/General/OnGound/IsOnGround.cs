﻿using UnityEngine;
using System.Collections;

public class IsOnGround : MonoBehaviour
{
    public bool onGround;
    public bool inAir;
    public string tagged;

    void OnCollisionStay(Collision otherThing)
    {
        if (otherThing.gameObject.tag == tagged)
        {
            onGround = true;
            inAir = false;
        }
    }

    void OnCollisionExit(Collision otherThing)
    {
        if (otherThing.gameObject.tag == tagged)
        {
            onGround = false;
            inAir = true;
        }
    }
}
