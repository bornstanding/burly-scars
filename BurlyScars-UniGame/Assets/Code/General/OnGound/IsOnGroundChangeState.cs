﻿using UnityEngine;
using System.Collections;

public class IsOnGroundChangeState : StateBase
{
    public IsOnGround isOnGround;
    public bool isInAir;
    public StateManager currentState;
    public StateManager nextState;

    public override void OnStateUpdate(Object sender)
    {
        base.OnStateUpdate(sender);

        if (isOnGround.inAir == isInAir)
        {
            currentState.ChangeState(nextState);
        }
    }
}
