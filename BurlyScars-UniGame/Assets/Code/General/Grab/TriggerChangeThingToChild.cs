using UnityEngine;
using System.Collections;

public class TriggerChangeThingToChild : MonoBehaviour
{
	public OSEnterTransformParent transformParent;
	
	void OnTriggerStay(Collider otherThing)
	{
		HelperComponent otherGrab = otherThing.gameObject.GetComponent<HelperComponent>();
		if(otherGrab != null)
		{
			transformParent.thingToChild = otherThing.gameObject;
		}
		
		return;
	}
	
	void OnTriggerExit(Collider otherThing)
	{
		HelperComponent otherGrab = otherThing.gameObject.GetComponent<HelperComponent>();
		if(otherGrab != null)
		{
			transformParent.thingToChild = null;;
		}
		
		return;
	}
}
