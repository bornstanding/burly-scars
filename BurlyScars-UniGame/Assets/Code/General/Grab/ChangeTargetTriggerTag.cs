﻿using UnityEngine;
using System.Collections;

public class ChangeTargetTriggerTag : MonoBehaviour 
{
	public AIFollow aiFollow;
	public string tagged;
	public string functionName;
	
	public event EventHandlers.MinimalHandler AttackHelper = delegate { };
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			aiFollow.target = otherThing.transform;
			AttackHelper();
		}
		else
		{
			return;
		}
	}
	
	void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			aiFollow.target = null;
		}
		else
		{
			return;
		}
	}
}
