using UnityEngine;
using System.Collections;

public class AddForceAddMinMaxForce : StateBase
{
	public AddMinMaxForce forceComponent;
	public OSEnterTransformParent objectToAddForce;
	//public float forceMulti;
	public Vector3 direction;
	public GameObject characetr;
	
	public override void OnStateEnter (Object sender)
	{
		direction = (characetr.transform.forward);
		//objectToAddForce.thingToChild.rigidbody.AddForce(direction.x * forceComponent.currentForce, direction.y * 0, direction.z * forceComponent.currentForce);
		objectToAddForce.thingToChild.rigidbody.AddForce(direction * forceComponent.currentForce);
	}
}
