using UnityEngine;
using System.Collections;

public class AddMinMaxForce : StateBase 
{
	public KeyCode key;
	public float currentForce;
	public float minForce;
	public float maxForce;
	public float addToTime;
	public float changeTimer;
	
	public override void OnStateEnter(Object sender)
	{
		currentForce = minForce;
	}
	
	void Update()
	{
		if(Input.GetKey(key))
		{
			currentForce += addToTime * Time.deltaTime;
			if(currentForce <= minForce)
			{
				addToTime = changeTimer;
			}
			
			if(currentForce >= maxForce)
			{
				addToTime = -changeTimer;
			}
		}
	}
}
