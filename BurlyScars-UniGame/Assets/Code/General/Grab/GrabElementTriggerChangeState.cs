﻿using UnityEngine;
using System.Collections;

public class GrabElementTriggerChangeState : MonoBehaviour 
{
	public StateManager normalState;
	public StateManager nextState;
	
	void OnTriggerEnter(Collider otherThing)
	{
		GrabElement otherGrabElement = otherThing.gameObject.GetComponent<GrabElement>();
		if(otherGrabElement != null)
		{
			normalState.ChangeState(nextState);
		}
		
		return;
	}
	
	void OnTriggerExit(Collider otherThing)
	{
		GrabElement otherGrabElement = otherThing.gameObject.GetComponent<GrabElement>();
		if(otherGrabElement != null)
		{
			nextState.ChangeState(normalState);
		}
		
		return;
	}
}
