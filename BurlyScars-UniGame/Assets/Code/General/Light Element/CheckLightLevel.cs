﻿using UnityEngine;
using System.Collections;

public class CheckLightLevel : MonoBehaviour
{
    public float lightLevel;

    void OnTriggerEnter(Collider otherThing)
    {
        LightLevel otherLightLevel = otherThing.gameObject.GetComponent<LightLevel>();
        if (otherLightLevel != null)
        {
            lightLevel = otherLightLevel.lightLevel;
        }

        else
        {
            return;
        }
    }
}
