﻿using UnityEngine;
using System.Collections;

public class CheckTorchChangeState : MonoBehaviour 
{
    public event EventHandlers.MinimalHandler InTorch = delegate { };
    public event EventHandlers.MinimalHandler ExitTorch = delegate { };

    void OnTriggerStay(Collider otherThing)
    {
        TorchLight otherTorchLight = otherThing.gameObject.GetComponent<TorchLight>();
        if(otherTorchLight != null)
        {
            print("In Torch");
            InTorch();
        }
    }

    void OnTriggerExit(Collider otherThing)
    {
        TorchLight otherTorchLight = otherThing.gameObject.GetComponent<TorchLight>();
        if (otherTorchLight != null)
        {
            print("Exit Torch");
            ExitTorch();
        }
    }
}
