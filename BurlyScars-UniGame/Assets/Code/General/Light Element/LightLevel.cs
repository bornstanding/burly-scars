﻿using UnityEngine;
using System.Collections;

public class LightLevel : MonoBehaviour
{
    public StandardHealth standardHealth;
    public float lightLevel;

    void Update()
    {
        lightLevel = standardHealth.currentHealth;
    }
}
