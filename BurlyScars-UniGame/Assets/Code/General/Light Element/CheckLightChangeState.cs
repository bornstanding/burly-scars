﻿using UnityEngine;
using System.Collections;

public class CheckLightChangeState : MonoBehaviour
{
    public event EventHandlers.MinimalHandler InLight = delegate { };
    public event EventHandlers.MinimalHandler ExitLight = delegate { };

    void OnTriggerStay(Collider otherThing)
    {
        LightElement otherLight = otherThing.gameObject.GetComponent<LightElement>();
        if(otherLight != null)
        {
            print("In Light");
            InLight();
        }
    }

    void OnTriggerExit(Collider otherThing)
    {
        LightElement otherLight = otherThing.gameObject.GetComponent<LightElement>();
        if (otherLight != null)
        {
            print("Exit Light");
            ExitLight();
        }
    }
}
