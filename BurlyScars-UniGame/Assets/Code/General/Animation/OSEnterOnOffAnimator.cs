﻿using UnityEngine;
using System.Collections;

public class OSEnterOnOffAnimator : StateBase 
{
	public Animator mainAnimator;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		mainAnimator.enabled = true;
	}

	public override void OnStateExit(Object sender)
	{
		base.OnStateExit(sender);
		
		mainAnimator.enabled = false;
	}
}
