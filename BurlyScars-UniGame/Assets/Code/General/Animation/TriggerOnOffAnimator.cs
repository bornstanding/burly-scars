﻿using UnityEngine;
using System.Collections;

public class TriggerOnOffAnimator : MonoBehaviour 
{
	public Animator mainAnimator;
	public bool enterOnOff;
	public bool exitOnOff;
	public string tagged;

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			mainAnimator.enabled = enterOnOff;
		}
	}

	public void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			mainAnimator.enabled = exitOnOff;
		}
	}


}
