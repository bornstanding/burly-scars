﻿using UnityEngine;
using System.Collections;

public class TriggerExitFlip : MonoBehaviour
{
    public CurrentRotation currentRotation;
    public string tagged;

    void OnTriggerExit(Collider otherThing)
	{
		if (otherThing.tag == tagged)
		{
			currentRotation.currentRotationY += 180;
		}

        else
        {
            return;
        }
    }
}
