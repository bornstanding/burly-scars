﻿using UnityEngine;
using System.Collections;

public class TriggerEnterFlip : MonoBehaviour
{
    public CurrentRotation currentRotation;
    public string tagged;

    void OnTriggerEnter(Collider otherThing)
    {
        if (otherThing.tag == tagged)
        {
            currentRotation.currentRotationY += 180;
        }

        else
        {
            return;
        }
    }
}
