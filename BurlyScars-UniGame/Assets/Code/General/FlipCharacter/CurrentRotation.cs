﻿using UnityEngine;
using System.Collections;

public class CurrentRotation : MonoBehaviour
{
    public float currentRotationY;
    public Rigidbody mainBody;

    void Update()
    {
        mainBody.transform.eulerAngles = new Vector3(0, currentRotationY, 0);
    }
}
