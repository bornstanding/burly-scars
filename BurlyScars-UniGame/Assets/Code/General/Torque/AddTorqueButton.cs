﻿using UnityEngine;
using System.Collections;

public class AddTorqueButton : MonoBehaviour
{
    public Rigidbody mainBody;
    public KeyCode rotateLeft;
    public KeyCode rotateRight;
    public Vector3 torqueSpeed;

    void Update()
    {
        if (Input.GetKey(rotateLeft))
        {
            mainBody.AddTorque(torqueSpeed);
        }

        if (Input.GetKey(rotateRight))
        {
            mainBody.AddTorque(-torqueSpeed);
        }
    }
}
