﻿using UnityEngine;
using System.Collections;

public class AddTorque : MonoBehaviour
{
    public GameObject mainObject;
    public Vector3 torqueSpeed;

    private void Update()
    {
        mainObject.rigidbody.AddTorque(torqueSpeed);
    }
}
