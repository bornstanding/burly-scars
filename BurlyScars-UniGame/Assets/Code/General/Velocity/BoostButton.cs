﻿using UnityEngine;
using System.Collections;

public class BoostButton : MonoBehaviour 
{
	public OSEnterChangeVelocity changeVelocity;
	public ChangeVelocityKeyCode changeVelocityKeyCode;
    public float boostAmount;
	public KeyCode boostKey;
	public float timer;
	public float restartTimer;

	void Update()
	{	
		timer -= 1 * Time.deltaTime;
		
		if(timer <= 0)
		{
			timer = 0;
			changeVelocityKeyCode.newZSpeed = changeVelocity.newSpeed;
		}
		
		if(Input.GetKeyDown(boostKey) && timer == 0)
		{
			changeVelocityKeyCode.newZSpeed = changeVelocityKeyCode.newZSpeed + boostAmount;
			timer = restartTimer;
		}
	}	
}
