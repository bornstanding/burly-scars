using UnityEngine;
using System.Collections;

public class AddVelocityForward : StateBase
{
	public float speed;
	public Rigidbody thingToSpawn;
	public GameObject spawnPoint;
	
	public override void OnStateEnter (Object sender)
	{
		Rigidbody clone;
		clone = Instantiate(thingToSpawn, spawnPoint.transform.position, spawnPoint.transform.rotation) as Rigidbody;
		clone.rigidbody.velocity = transform.forward * speed;
	}
}
