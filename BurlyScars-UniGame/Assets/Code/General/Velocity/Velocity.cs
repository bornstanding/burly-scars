using UnityEngine;
using System.Collections;

public class Velocity : MonoBehaviour 
{
	public Rigidbody parentBody;
	public Vector3 speed;
	
	void Update()
	{
		parentBody.velocity = new Vector3(speed.x,speed.y,speed.z);
	}
}
