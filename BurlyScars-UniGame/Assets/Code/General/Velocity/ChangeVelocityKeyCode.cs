﻿using UnityEngine;
using System.Collections;

public class ChangeVelocityKeyCode : MonoBehaviour 
{
	public Velocity velocity;
	public Rigidbody mainBody;
	public float newYSpeed;
	public float newZSpeed;
	public KeyCode upButton;
	public KeyCode downButton;
	
	void Update()
	{
		velocity.speed.z = newZSpeed;
		
		if(Input.GetKey(upButton))
		{
		    velocity.speed.y = newYSpeed;
		}
		
		if(Input.GetKey(downButton))
		{
		    velocity.speed.y = -newYSpeed;
		}
		
		if(Input.GetKeyUp(upButton) || Input.GetKeyUp(downButton))
		{
		    velocity.speed.y = 0;
		}
	}
}
