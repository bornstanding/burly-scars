using UnityEngine;
using System.Collections;

public class AddVelocity : MonoBehaviour 
{
	public float speed;
	public GameObject parentBody;
    public string tagged;

    private void Awake()
    {
        parentBody = GameObject.FindGameObjectWithTag(tagged) as GameObject;
    }

    void Update()
	{
		parentBody.rigidbody.velocity = transform.TransformDirection(Vector3.forward * speed);
	}
}
