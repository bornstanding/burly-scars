﻿using UnityEngine;
using System.Collections;

public class TagNegVelocity : MonoBehaviour
{
    public string tagged;

    private void OnCollisionEnter(Collision otherThing)
    {
        if(otherThing.gameObject.tag == tagged)
        {
            otherThing.rigidbody.velocity = -otherThing.rigidbody.velocity;
        }
    }
}
