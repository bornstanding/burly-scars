﻿using UnityEngine;
using System.Collections;

public class CollisionChangeVelocity : MonoBehaviour
{
    private void OnCollisionEnter(Collision otherThing)
    {
        Velocity otherVelocity = otherThing.gameObject.GetComponent<Velocity>();
        if (otherVelocity != null)
        {
            otherVelocity.speed = -otherVelocity.speed;
        }
    }
}
