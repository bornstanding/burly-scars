﻿using UnityEngine;
using System.Collections;

public class IsKinematicButton : MonoBehaviour 
{
	public Rigidbody mainBody;
	public KeyCode isKinematicOn;
	public KeyCode isKinematicOff;
	
	void Update()
	{
		if(Input.GetKeyDown(isKinematicOn))
		{
			mainBody.isKinematic = true;
		}
		
		if(Input.GetKeyDown(isKinematicOff))
		{
			mainBody.isKinematic = false;
		}
	}
}
