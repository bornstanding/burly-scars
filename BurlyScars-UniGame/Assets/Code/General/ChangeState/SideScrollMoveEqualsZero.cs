﻿using UnityEngine;
using System.Collections;

public class SideScrollMoveEqualsZero : MonoBehaviour
{
    public SideScrollMovement sideScrollMovement;
    public StateManager currentState;
    public StateManager nextState;

    void Update()
    {
        if (sideScrollMovement.moveSpeed == 0)
        {
            currentState.ChangeState(nextState);
        }
    }
}
