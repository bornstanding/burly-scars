﻿using UnityEngine;
using System.Collections;

public class CollisonInputSimple : MonoBehaviour 
{
    public string tagged;
    public KeyCode buttonCode;
    public StateManager currentState;
    public StateManager nextState;

    void OnCollisionStay(Collision otherThing)
    {
        if (otherThing.gameObject.tag == tagged)
        {
            if (Input.GetKeyDown(buttonCode))
            {
                currentState.ChangeState(nextState);
            }
        }
    }
}
