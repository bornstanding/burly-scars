﻿using UnityEngine;
using System.Collections;

public class HealthChangeStateMin : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public StateManager currentState;
	public StateManager nextState;
	public float minHealth;
	
	void Update()
	{
		if(standardHealth.currentHealth <= minHealth)
		{
			currentState.ChangeState(nextState);
		}
	}
}
