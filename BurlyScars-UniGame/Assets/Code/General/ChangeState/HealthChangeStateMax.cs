﻿using UnityEngine;
using System.Collections;

public class HealthChangeStateMax : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public StateManager currentState;
	public StateManager nextState;
	public float maxHealth;
	
	void Update()
	{
		if(standardHealth.currentHealth >= maxHealth)
		{
			currentState.ChangeState(nextState);
		}
	}
}
