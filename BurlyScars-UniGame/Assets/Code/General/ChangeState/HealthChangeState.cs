﻿using UnityEngine;
using System.Collections;

public class HealthChangeState : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public float greaterHealth;
	public float lessHealth;
	public StateManager currentState;
	public StateManager greaterState;
	public StateManager lessState;
	
	void Update()
	{
		if(standardHealth.currentHealth == greaterHealth)
		{
			currentState.ChangeState(greaterState);
		}
		
		if(standardHealth.currentHealth == lessHealth)
		{
			currentState.ChangeState(lessState);
		}
		
		if(greaterState == null || greaterHealth ==null)
		{
			return;
		}
	}
}
