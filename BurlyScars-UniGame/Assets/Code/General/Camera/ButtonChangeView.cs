﻿using UnityEngine;
using System.Collections;

public class ButtonChangeView : StateBase
{
	public CameraFollow followCamera;
	public Vector3 newView;

	public override void OnStateEnter (Object sender)
	{
		followCamera.offset = new Vector3(newView.x, newView.y, newView.z);
	}
}
