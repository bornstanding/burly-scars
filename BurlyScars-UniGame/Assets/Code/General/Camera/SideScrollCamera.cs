﻿using UnityEngine;
using System.Collections;

public class SideScrollCamera : MonoBehaviour
{
    public GameObject followObject;
    public Vector3 positionWithinCamera;
    public float widthMiddle;
    public float heightMiddle;
    public float rightWall;
    public float wallOffset;
    public Vector3 cameraOffset;
    public Vector3 playerPositionY;
    public KeyCode moveAwayKey;

    void Awake()
    {
        // World Float 
        //widthMiddle = camera.pixelWidth/2;
        //heightMiddle = camera.pixelHeight/2;

        // World Pixels (Int)
        widthMiddle = Screen.width/2;
        heightMiddle = Screen.height/2;
        rightWall = Screen.width + wallOffset; 
    }
    
    void FixedUpdate()
    {
        //Vector3 screenPos = camera.WorldToScreenPoint(followObject.transform.position);
        positionWithinCamera = camera.WorldToScreenPoint(followObject.transform.position);

        //Camera to follow the players Y Position
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x , Mathf.Lerp(Camera.main.transform.position.y, followObject.transform.position.y + 4, 2 * Time.deltaTime), -20);

        //camera.transform.position.y = followObject.transform.position;

        if (positionWithinCamera.x <= widthMiddle)
        {
            print("Camera follow player");
            Camera.main.transform.position = new Vector3(Mathf.Lerp(Camera.main.transform.position.x, followObject.transform.position.x, 2 * Time.deltaTime), Mathf.Lerp(Camera.main.transform.position.y, followObject.transform.position.y + 4, 2 * Time.deltaTime), -20);
        }

        if (positionWithinCamera.x >= rightWall)
        {
            positionWithinCamera.x = rightWall;
            followObject.rigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }

        if (Input.GetKey(moveAwayKey))
        {
            followObject.rigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
    }
}
