﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour 
{
	public GameObject checkPointPosition;
	//public GameObject thingToSpawn;
	public string checkPointTag;
	public StandardHealth standardHealth;

	void Start()
	{
		checkPointPosition = GameObject.FindGameObjectWithTag(checkPointTag);
	}

	public void MoveToCheckPoint()
	{
		//GameObject clone;
		//clone = Instantiate(thingToSpawn, checkPointPosition.transform.position, checkPointPosition.transform.rotation) as GameObject;

		gameObject.transform.position = checkPointPosition.transform.position;
		standardHealth.currentHealth = standardHealth.maxHealth;
	}
}
