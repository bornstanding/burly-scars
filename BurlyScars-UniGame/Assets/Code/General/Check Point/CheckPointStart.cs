﻿using UnityEngine;
using System.Collections;

public class CheckPointStart : MonoBehaviour 
{
	public GameObject checkPointPosition;
	public GameObject thingToSpawn;
	public string checkPointTag;
	
	void Start()
	{
		checkPointPosition = GameObject.FindGameObjectWithTag(checkPointTag);
		MoveToCheckPoint();
	}

	public void MoveToCheckPoint()
	{
		GameObject clone;
		clone = Instantiate(thingToSpawn, checkPointPosition.transform.position, checkPointPosition.transform.rotation) as GameObject;
	}
}
