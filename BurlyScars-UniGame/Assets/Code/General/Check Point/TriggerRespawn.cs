﻿using UnityEngine;
using System.Collections;

public class TriggerRespawn : MonoBehaviour 
{
	public CheckPoint checkPointCode;
	public string tagged;

	void OnTriggerEnter(Collider otherThing)
	{
		checkPointCode = GameObject.FindObjectOfType(typeof(CheckPoint)) as CheckPoint;
		if(otherThing.tag == tagged)
		{
			checkPointCode.MoveToCheckPoint();
		}
	}
}
