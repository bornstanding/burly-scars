﻿using UnityEngine;
using System.Collections;

public class AmmoGrass : MonoBehaviour 
{
	public Ammo ammo;
	public float currentAmmo;
	
	void Awake()
	{
		currentAmmo = ammo.ammoAmount;
	}
}
