﻿using UnityEngine;
using System.Collections;

public class AmmoFire : MonoBehaviour 
{
	public Ammo ammo;
	public float currentAmmo;
	
	void Awake()
	{
		currentAmmo = ammo.ammoAmount;
	}
}
