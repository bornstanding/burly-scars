﻿using UnityEngine;
using System.Collections;

public class CalculateAmmoAmount : MonoBehaviour 
{
	public float ammoAmount;
	public AmmoAmount amount;
	
	void OnTriggerEnter(Collider otherThing)
	{
		HealthPercentageMinEvent percentageAmmo = otherThing.gameObject.GetComponent<HealthPercentageMinEvent>();
		if(percentageAmmo != null)
		{
			ammoAmount = Mathf.CeilToInt((percentageAmmo.currentPercent * amount.ammoAmount) / 100);
		}
		
		else 
		{
			return;
		}
	}
}
