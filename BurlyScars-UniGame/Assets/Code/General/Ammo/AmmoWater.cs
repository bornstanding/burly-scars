﻿using UnityEngine;
using System.Collections;

public class AmmoWater : MonoBehaviour 
{
	public Ammo ammo;
	public float currentAmmo;
	
	void Awake()
	{
		currentAmmo = ammo.ammoAmount;
	}
}
