﻿using UnityEngine;
using System.Collections;

public class OSUpdateAmmo : StateBase 
{
	public CalculateAmmoAmount calculate;
	public Ammo ammo;
	
	public override void OnStateUpdate(Object sender)
	{
		ammo.ammoAmount = calculate.ammoAmount;
	}
}
