﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeAmmoAmount : StateBase
{
	public AmmoAmount ammoAmount;
	public CalculateAmmoAmount calculate;
	
	public override void OnStateEnter(Object sender)
	{
		calculate.amount = ammoAmount;
	}
}
