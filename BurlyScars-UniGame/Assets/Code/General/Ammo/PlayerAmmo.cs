﻿using UnityEngine;
using System.Collections;

public class PlayerAmmo : MonoBehaviour 
{
	public StandardHealth fireHealth;
	public StandardHealth grassHealth;
	public StandardHealth waterHealth;
	
	void OnCollisionEnter(Collision otherThing)
	{
		AmmoFire playersFireAmmo = otherThing.gameObject.GetComponent<AmmoFire>();
		if(playersFireAmmo != null)
		{
			fireHealth.currentHealth += playersFireAmmo.currentAmmo;
		}
		
		AmmoGrass playersGrassAmmo = otherThing.gameObject.GetComponent<AmmoGrass>();
		if(playersGrassAmmo != null)
		{
			grassHealth.currentHealth += playersGrassAmmo.currentAmmo;
		}
		
		AmmoWater playersWaterAmmo = otherThing.gameObject.GetComponent<AmmoWater>();
		if(playersWaterAmmo != null)
		{
			waterHealth.currentHealth += playersWaterAmmo.currentAmmo;
		}
		
		else 
		{
			return;
		}
	}
}
