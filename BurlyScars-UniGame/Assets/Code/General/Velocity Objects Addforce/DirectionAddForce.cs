﻿using UnityEngine;
using System.Collections;

public class DirectionAddForce : StateBase
{
	public Rigidbody mainBody;
	public float forceAmount;
	public Vector3 forceDirection;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		mainBody.AddForce(transform.TransformDirection(forceDirection * forceAmount));
	}
}
