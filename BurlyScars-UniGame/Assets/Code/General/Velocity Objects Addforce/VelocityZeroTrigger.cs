﻿using UnityEngine;
using System.Collections;

public class VelocityZeroTrigger : MonoBehaviour
{
    public SideScrollMovement sideScrollMovement;

    void OnTriggerEnter(Collider otherThing)
    {
        AddforceObject otherForceObject = otherThing.gameObject.GetComponent<AddforceObject>();
        if (otherForceObject != null)
        {
            sideScrollMovement.moveSpeed = 0;
        }
    }
}
