﻿using UnityEngine;
using System.Collections;

public class UpdateDirectionalForce : MonoBehaviour 
{
	public Rigidbody mainBody;
	public float forceAmount;
	public Vector3 forceDirection;
	
	void Update()
	{
		mainBody.AddForce(transform.TransformDirection(forceDirection * forceAmount));
	}
}
