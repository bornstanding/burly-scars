using UnityEngine;
using System.Collections;

public class NGuiSimpleScoreSystem : MonoBehaviour 
{
	public SimpleScoreSystem simpleScoreSystem;
	public UILabel uILabel;
	public string labelInfo;
	
	void Update()
	{
		uILabel.text = labelInfo + simpleScoreSystem.Score.ToString("f0");
	}	
}
