using UnityEngine;
using System.Collections;

public class SimpleScoreSystem : MonoBehaviour 
{
	public float Score;
    public float addAmount;
    public float minusAmount;
	
	public void AddToScore()
	{
		Score += addAmount;
	}
	
	public void MinusToScore()
	{
		Score -= minusAmount;
	}
}
