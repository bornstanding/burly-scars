using UnityEngine;
using System.Collections;

public class OSEnterScoreSystemMod : StateBase
{
    public SimpleScoreSystem simpleScoreSystem;
    public float newAddAmount;
    public float newMinusAmaount; 

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);
        simpleScoreSystem.addAmount = newAddAmount;
        simpleScoreSystem.minusAmount = newMinusAmaount;
        simpleScoreSystem.AddToScore();
        simpleScoreSystem.MinusToScore();
    }
}
