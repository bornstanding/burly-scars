using UnityEngine;
using System.Collections;

public class ScoreFunctions : MonoBehaviour 
{
	public float score;
	public float addScore;
	public float minusScore;
	public float timesAddScore;
	public float timesScore;
	
	public void AddToScore()
	{
		score += addScore;
	}
	
	public void MinusTheScore()
	{
		score -= minusScore;
	}
	
	public void TimesTheAddScoreToScore()
	{
		score += addScore * timesAddScore;
	}
	
	public void TimesTheScore()
	{
		score = score * timesScore;
	}
}
