﻿using UnityEngine;
using System.Collections;

public class FindScoreSystemSimple : MonoBehaviour
{
    public OSEnterScoreSystemMod systemMod;

    private void Awake()
    {
        systemMod.simpleScoreSystem = GameObject.FindObjectOfType(typeof(SimpleScoreSystem)) as SimpleScoreSystem;
    }
}
