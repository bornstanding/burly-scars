﻿using UnityEngine;
using System.Collections;

public class RandomLerpMovement : MonoBehaviour
{
    public GameObject mainObject;
    public float minRange;
    public float maxRange;
    public float randomNumber;
    public float yLerp;
    public float zLerp;

    void Awake()
    {
        randomNumber = Random.Range(minRange, maxRange);
    }

    void Update()
    {
        if (mainObject.transform.position.x != randomNumber)
        {
            moveTowardsRandom();
        }

        if (mainObject.transform.position.x == randomNumber)
        {
            randomNumber = Random.Range(minRange, maxRange);
        }
    }

    public void moveTowardsRandom()
    {
        mainObject.transform.position = new Vector3(Mathf.MoveTowards(mainObject.transform.position.x, randomNumber, 1 * Time.time), yLerp, zLerp);
    }
}
