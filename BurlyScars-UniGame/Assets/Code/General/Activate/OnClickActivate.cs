using UnityEngine;
using System.Collections;

public class OnClickActivate : MonoBehaviour 
{
	public IsActivated isActivated;
	
	void OnMouseDown()
	{
		isActivated.isActivated = true;
	}
}
