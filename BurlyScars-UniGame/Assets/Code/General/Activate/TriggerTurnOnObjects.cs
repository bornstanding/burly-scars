﻿using UnityEngine;
using System.Collections;

public class TriggerTurnOnObjects : MonoBehaviour
{
    public GameObject[] turnOnObjects;
    public string tagged;

    void OnTriggerEnter(Collider otherThing)
    {
        if (otherThing.tag == tagged)
        {
            foreach (GameObject item in turnOnObjects)
            {
                item.SetActive(true);
                Destroy(gameObject);
            }
        }
    }
}
