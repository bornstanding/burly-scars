﻿using UnityEngine;
using System.Collections;

public class AddForceAngle : StateBase
{
    public CalcAngle calcAngle;
    public Rigidbody thingToSpawn;
    public GameObject spawnPoint;
    public float forceAmount;
    public float maxForce;
    public float minForce;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        forceAmount = calcAngle.distance;
        if (forceAmount >= maxForce)
        {
            forceAmount = maxForce;
        }

        Rigidbody clone;
        clone = Instantiate(thingToSpawn, spawnPoint.transform.position, spawnPoint.transform.rotation) as Rigidbody;
        clone.rigidbody.velocity = calcAngle.angle * (forceAmount + minForce);
    }
}


