﻿using UnityEngine;
using System.Collections;

public class CalcAngle : StateBase
{
    public Transform playerTransform;
    public FindGameWithTagAwake aimTransform;
    public Vector3 angle;
    public float distance;

    public override void OnStateUpdate(Object sender)
    {
        base.OnStateUpdate(sender);

		if(aimTransform.objectWithTag != null)
		{
			angle = (aimTransform.objectWithTag.transform.position - playerTransform.position).normalized;
	        distance = Vector3.Distance(playerTransform.position, aimTransform.objectWithTag.transform.position);
		}

		else
		{
			return;
		}
    }
}
