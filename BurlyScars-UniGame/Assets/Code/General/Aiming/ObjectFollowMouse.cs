﻿using UnityEngine;
using System.Collections;

public class ObjectFollowMouse : MonoBehaviour
{
    public float speed;
    public Vector3 targetPos;

    private void Start()
    {
        targetPos = transform.position;
    }

    void Update()
    {
        //if (Input.GetMouseButton(0))
        //{
            float distance = transform.position.z - Camera.main.transform.position.z;
            targetPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            targetPos = Camera.main.ScreenToWorldPoint(targetPos);
        //}

        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed*Time.deltaTime);
    }
}