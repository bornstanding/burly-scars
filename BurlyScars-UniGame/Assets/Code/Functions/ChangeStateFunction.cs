﻿using UnityEngine;
using System.Collections;

public class ChangeStateFunction : MonoBehaviour 
{
	public StateManager currentState;
	public StateManager nextState;
	
	public void ChangeTheState()
	{
		currentState.ChangeState(nextState);
	}
}
