﻿using UnityEngine;
using System.Collections;

public class AttackHelpFunction : MonoBehaviour 
{
	public StateManager currentState;
	public StateManager nextState;
	
	public void AttackHelper()
	{
		currentState.ChangeState(nextState);
	}
}
