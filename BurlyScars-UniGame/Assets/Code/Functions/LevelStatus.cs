﻿using UnityEngine;
using System.Collections;

public class LevelStatus : MonoBehaviour 
{
	public string changeLevel;
	public float changeTimeScale;
	
	public void LoadCurrentLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
	
	public void ChangeLevel()
	{
		Application.LoadLevel(changeLevel);
	}
	
	public void Pause()
	{
		Time.timeScale = 0;
	}
	
	public void Unpause()
	{
		Time.timeScale = 1;
	}
	
	public void ChangeTimeScale()
	{
		Time.timeScale = changeTimeScale;
	}
}
