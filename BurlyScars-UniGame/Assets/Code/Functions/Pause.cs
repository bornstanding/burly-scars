using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour 
{
	public void PauseFunction()
	{
		Time.timeScale = 0;
	}
}
