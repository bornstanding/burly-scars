﻿using UnityEngine;
using System.Collections;

public class HealthModEventSub : MonoBehaviour
{
    public StandardHealth standardHealth;
    public float healthMod;
    public float timer;
    public float restartTimer;
    
   /* void Awake()
    {
        standardHealth = GameObject.FindObjectOfType(StandardHealth) as StandardHealth;
    }*/

    void OnEnable()
    {
        EventLightCollision.attackPlayer += HealthMod;
    }

    void OnDisable()
    {
        EventLightCollision.attackPlayer -= HealthMod;
    }

    void HealthMod(Collider otherThing)
    {
        timer -= 1*Time.deltaTime;
        if (timer <= 0)
        {
            standardHealth.currentHealth += healthMod;
            timer = restartTimer;
        }
    }
}
