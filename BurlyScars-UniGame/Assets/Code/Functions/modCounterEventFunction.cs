﻿using UnityEngine;
using System.Collections;

public class modCounterEventFunction : StateBase
{
	public CounterEventsThree counterEventsThree;
	public float modAmount;
	
	void Awake()
	{
		counterEventsThree = GameObject.FindObjectOfType(typeof(CounterEventsThree)) as CounterEventsThree;
	}
	
	public void addAmount()
	{
		counterEventsThree.currentCounter += modAmount;
	}
	
	public void minusAmount()
	{
		counterEventsThree.currentCounter -= modAmount;
	}
}
