﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayAudio : MonoBehaviour 
{
	public List<AudioClip> soundList = new List<AudioClip>();
	private AudioClip sound;
	public AudioSource source;
	public int audioListNumber;

	public void PlayShootAudio()
	{
		sound = soundList[audioListNumber];

		source.clip = sound;
		source.Play();
	}
}
