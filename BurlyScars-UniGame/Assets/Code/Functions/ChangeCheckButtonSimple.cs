﻿using UnityEngine;
using System.Collections;

public class ChangeCheckButtonSimple : MonoBehaviour 
{
	public  CheckButtonTransitionSimple checkSimple;
	public KeyCode newkey;
	
	public void ChangeCheckSimpleKey()
	{
		checkSimple.key = newkey;
	}
}
