using UnityEngine;
using System.Collections;

public class TransformParent : StateBase
{
	public GameObject player;
	
	public void TurnIntoChild()
	{
		if (rootIdentity != null) 
        rootIdentity.transform.parent = player.transform;
	}
	
	public void UnChild()
	{
		transform.parent = null;
	}
}
