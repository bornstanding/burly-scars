using UnityEngine;
using System.Collections;

public class SetActive : MonoBehaviour 
{
	public bool onOrOff;
	public GameObject thingToActive;
	
	public void SetActiveFunction()
	{
		thingToActive.SetActive(onOrOff);
	}
}
