using UnityEngine;
using System.Collections;

public class HealthModFunctions : MonoBehaviour 
{
	public StandardHealth standardHealth;
	public float modAmount;
	
	public void AddHealth()
	{
		standardHealth.currentHealth += modAmount;
	}
	
	public void MinusHealth()
	{
		standardHealth.currentHealth -= modAmount;
	}
}
