﻿using UnityEngine;
using System.Collections;

public class ChangeInputButtonSimple : MonoBehaviour 
{
	public  InputTransitionSimple inputSimple;
	public KeyCode newkey;
	
	public void ChangeInputSimpleKey()
	{
		inputSimple.key = newkey;
	}
}
