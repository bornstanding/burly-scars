﻿using UnityEngine;
using System.Collections;

public class ChangeTargetFunction : MonoBehaviour 
{
	public AIFollow aiFollow;
	public GameObject newTarget;
	public string tagged;
	
	void Awake()
	{
		newTarget = GameObject.FindWithTag(tagged);
	}
	
	public void ChangeTarget()
	{
		aiFollow.target = newTarget.transform;
	}
}
