using UnityEngine;
using System.Collections;

public class OnCollisionNextState : MonoBehaviour
{
	public StateManager currentState;
	public StateManager nextState;
	public string tagged;

	void OnCollisionEnter(Collision otherThing)
	{
		if(otherThing.gameObject.tag == tagged)
		{
			currentState.ChangeState(nextState);
		}
	}
}
