using UnityEngine;
using System.Collections;

public class OnClickNextState : StateBase
{
	public StateManager currentState;
	
	void OnMouseDown()
	{
		currentState.NextState();
	}
}
