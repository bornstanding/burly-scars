﻿using UnityEngine;
using System.Collections;

public class ChangeCurrentStateFunction : MonoBehaviour 
{
	public CurrentState currentstate;
	public StateManager nextState;
	
	public void ChangeCurrentState()
	{
		currentstate.currentState.ChangeState(nextState);
	}
}
