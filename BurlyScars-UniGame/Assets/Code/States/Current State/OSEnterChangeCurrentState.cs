﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeCurrentState : StateBase
{
	public CurrentState currentState;
	public StateManager newCurrentState;
	
	public override void OnStateEnter (Object sender)
	{
		currentState.currentState = newCurrentState;
	}
}
