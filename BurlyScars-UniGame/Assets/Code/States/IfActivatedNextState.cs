using UnityEngine;
using System.Collections;

public class IfActivatedNextState : StateBase
{
	public IsActivated isActivated;
	public StateManager changeState;
	
	void Update()
	{
		if(isActivated.isActivated == true)
		{
			changeState.NextState();
		}
	}
}
