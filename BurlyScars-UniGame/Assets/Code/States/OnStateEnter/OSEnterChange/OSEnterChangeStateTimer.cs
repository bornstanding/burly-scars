using UnityEngine;
using System.Collections;

public class OSEnterChangeStateTimer : StateBase 
{
	public TimerChangeState timerChangeState;
	public float changeTimer;
	public float changeRestartTimer;
	
	public override void OnStateEnter (Object sender)
	{
		timerChangeState.timer = changeTimer;
		timerChangeState.restartTimer = changeRestartTimer;
	}
}
