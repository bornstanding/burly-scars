﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeVelocity : StateBase
{
	public ChangeVelocityKeyCode velocityKeyCodes;
	public float newSpeed;
	
	public override void OnStateEnter(Object sender)
	{
		velocityKeyCodes.newYSpeed = newSpeed;
		velocityKeyCodes.newZSpeed = newSpeed;
	}
}
