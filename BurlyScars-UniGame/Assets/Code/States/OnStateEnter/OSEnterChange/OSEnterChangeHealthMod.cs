﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeHealthMod : StateBase
{
	public OSEnterHealthMod healthMod;
	public StandardHealth newStandardHealth;
	public float newHealthAmount;
	
	public override void OnStateEnter (Object sender)
	{
		healthMod.standardHealth = newStandardHealth;
		healthMod.healthAmount = newHealthAmount;
	}
}
