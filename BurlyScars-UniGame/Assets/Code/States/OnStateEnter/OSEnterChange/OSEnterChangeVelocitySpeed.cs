﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeVelocitySpeed : StateBase
{
    public Vector3 newSpeed;
    public Velocity mainVelocity;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        mainVelocity.speed = newSpeed;
    }
}
