﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeDamage : StateBase
{
    public CollisionTriggerTagHealthMod healthMod;
    public float newDamage;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);
        healthMod.modHealth = newDamage;
    }
}
