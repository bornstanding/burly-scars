using UnityEngine;
using System.Collections;

public class OnStateEnterChangeBullet : StateBase
{
	public AddVelocityForward addVelocityForward;
	public Rigidbody changeBullet;
	public float changeBulletSpeed;
	
	public override void OnStateEnter (Object sender)
	{
		addVelocityForward.thingToSpawn = changeBullet;
		addVelocityForward.speed = changeBulletSpeed;
	}
}
