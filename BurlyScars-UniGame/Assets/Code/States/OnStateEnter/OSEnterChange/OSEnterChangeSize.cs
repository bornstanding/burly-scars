﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeSize : StateBase
{
    public GameObject viewObject;
    public SphereCollider colliderSphere;
    public Vector3 newVector3;
    public float newRadius;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        viewObject.transform.localScale = (newVector3);

        if (colliderSphere != null)
        {
            colliderSphere.radius = (newRadius);
        }

        else
        {
            return;
        }
    }
}
