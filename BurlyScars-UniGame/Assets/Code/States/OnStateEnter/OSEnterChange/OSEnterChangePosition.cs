using UnityEngine;
using System.Collections;

public class OSEnterChangePosition : StateBase 
{
	public CurrentStartPosition currentStartPosition;
	public GameObject objectThing;
	
	public override void OnStateEnter (Object sender)
	{
		objectThing.transform.position = currentStartPosition.sPosition;
	}
}
