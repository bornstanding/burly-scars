﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeSimpleCheckKey : StateBase
{
	public CheckButtonTransitionSimple checkSimple;
	public KeyCode newKey;
	
	public override void OnStateEnter (Object sender)
	{
		checkSimple.key = newKey;
	}
}
