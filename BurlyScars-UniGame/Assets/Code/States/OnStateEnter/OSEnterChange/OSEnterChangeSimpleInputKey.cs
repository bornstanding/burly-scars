﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeSimpleInputKey : StateBase
{
	public InputTransitionSimple inputSimple;
	public KeyCode newKey;
	
	public override void OnStateEnter (Object sender)
	{
		inputSimple.key = newKey;
	}
}
