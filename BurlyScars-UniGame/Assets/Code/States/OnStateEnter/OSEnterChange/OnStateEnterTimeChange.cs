using UnityEngine;
using System.Collections;

public class OnStateEnterTimeChange : StateBase
{
	public float newTimeScale;
	
	public override void OnStateEnter (Object sender)
	{
		Time.timeScale = newTimeScale;
	}
}
