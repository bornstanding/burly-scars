﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeTorque : StateBase
{
    public AddTorque addTorque;
    public Vector3 newTorque;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        addTorque.torqueSpeed = newTorque;
    }
}
