﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeShootDirection : StateBase
{
    public OSEnterShootDirection shootDirection;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    public float zMin;
    public float zMax;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        shootDirection.addAngle.x = Random.Range(xMin, xMax);
        shootDirection.addAngle.y = Random.Range(yMin, yMax);
        shootDirection.addAngle.z = Random.Range(zMin, zMax);
    }
}
