﻿using UnityEngine;
using System.Collections;

public class OSEnterHealthModDamage : StateBase
{
    public HealthModTriggersCollisionTag healthMod;
    public float newModHealthAmount;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        healthMod.modHealth = newModHealthAmount;
    }
}
