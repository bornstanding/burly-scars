﻿using UnityEngine;
using System.Collections;

public class OSEnterAIChangeSpeed : StateBase 
{
	public AIFollow aiFollow;
	public float newSpeed;
	
	public override void OnStateEnter (Object sender)
	{
		aiFollow.speed = newSpeed;
	}
}
