using UnityEngine;
using System.Collections;

public class OSEnterOverlapSphere : StateBase 
{
	public float radius;
	public string functionName;
	
    public override void OnStateEnter (Object sender) 
	{
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        int i = 0;
        while (i < hitColliders.Length) 
		{
			//Debug.LogWarning(hitColliders[i].name);
			hitColliders[i].SendMessage(functionName,SendMessageOptions.DontRequireReceiver);
            i++;
        }
    }
}
