using UnityEngine;
using System.Collections;

public class OnStateEnterTimeNormal : StateBase 
{
	public override void OnStateEnter (Object sender)
	{
		Time.timeScale = 1;
	}
}
