﻿using UnityEngine;
using System.Collections;

public class OSEnterSendMessage : StateBase
{
    public string functionName;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        SendMessage(functionName, SendMessageOptions.RequireReceiver);
    }
}
