﻿using UnityEngine;
using System.Collections;

public class OSEnterDestroy : StateBase 
{
	public GameObject destroyThing;
	
	public override void OnStateEnter (Object sender)
	{
		if(destroyThing!=null) 
		{
			Destroy(destroyThing);
		}

		else
		{
		    return;
		}
	}
}
