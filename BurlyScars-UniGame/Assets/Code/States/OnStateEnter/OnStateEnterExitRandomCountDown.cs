using UnityEngine;
using System.Collections;

public class OnStateEnterExitRandomCountDown : StateBase
{
	public float timer;
	public int minRestartTimer;
	public int maxRestartTimer;
	
	public override void OnStateEnter (Object sender)
	{
		CountDown();
	}
	
	public override void OnStateExit (Object sender)
	{
		timer = Random.Range(minRestartTimer, maxRestartTimer);
	}
	
	void Update()
	{
		CountDown();
	}
	
	public void CountDown()
	{
		timer -= 1 * Time.deltaTime;
		if(timer <= 0)
		{
			stateManager.NextState();
		}
	}
}
