using UnityEngine;
using System.Collections;

public class OnStateEnterRandomVelocity : StateBase
{
	public Velocity velocity;
	public int minX;
	public int maxX;
	public int minY;
	public int maxY;
	public int minZ;
	public int maxZ;
	
	public override void OnStateEnter (Object sender)
	{
		velocity.speed = new Vector3(Random.Range(minX, maxX),Random.Range(minY, maxY), Random.Range(minZ, maxZ));
	}
}
