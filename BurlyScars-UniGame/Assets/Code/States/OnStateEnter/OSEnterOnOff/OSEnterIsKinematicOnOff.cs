using UnityEngine;
using System.Collections;

public class OSEnterIsKinematicOnOff : StateBase 
{
	public Rigidbody parentBody;
	public bool isKinematicOnOff;
	
	public override void OnStateEnter (Object sender) 
	{
		parentBody.isKinematic = isKinematicOnOff;
	}
}
