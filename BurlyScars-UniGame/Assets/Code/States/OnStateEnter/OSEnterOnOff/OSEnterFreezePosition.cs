﻿using UnityEngine;
using System.Collections;

public class OSEnterFreezePosition : StateBase
{
    public Rigidbody mainBody;
    public bool xRotationPosition;
    public bool yRotationPosition;
    public bool zRotationPosition;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        if (!xRotationPosition && !yRotationPosition && !zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.None;
        }

        if (xRotationPosition && !yRotationPosition && !zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.FreezePositionX;
        }

        if (!xRotationPosition && yRotationPosition && !zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.FreezePositionY;
        }

        if (!xRotationPosition && !yRotationPosition && zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.FreezePositionZ;
        }

        if (xRotationPosition && yRotationPosition && !zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
        }

        if (xRotationPosition && !yRotationPosition && zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        }

        if (!xRotationPosition && yRotationPosition && zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        }

        if (xRotationPosition && yRotationPosition && zRotationPosition)
        {
            mainBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        }
    }
}
