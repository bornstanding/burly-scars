using UnityEngine;
using System.Collections;

public class OSEnterGravityOnOff : StateBase 
{
	public Rigidbody parentBody;
	public bool gravityOnOff;
	
	public override void OnStateEnter (Object sender) 
	{
		parentBody.useGravity = gravityOnOff;
	}
}
