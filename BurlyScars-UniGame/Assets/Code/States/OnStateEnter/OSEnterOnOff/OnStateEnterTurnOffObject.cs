using UnityEngine;
using System.Collections;

public class OnStateEnterTurnOffObject : StateBase 
{
	public GameObject thingToTurnOff;
	
	public override void OnStateEnter (Object sender)
	{
		thingToTurnOff.SetActive(false);
	}
}
