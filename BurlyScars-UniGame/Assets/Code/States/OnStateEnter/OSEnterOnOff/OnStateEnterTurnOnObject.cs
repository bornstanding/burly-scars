using UnityEngine;
using System.Collections;

public class OnStateEnterTurnOnObject : StateBase 
{
	public GameObject thingToTurnOn;
	
	public override void OnStateEnter (Object sender)
	{
		thingToTurnOn.SetActive(true);
	}
}
