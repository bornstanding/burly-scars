﻿using UnityEngine;
using System.Collections;

public class OSEnterRigidBodyConstraints : StateBase
{
    public Rigidbody mainBody;
    public bool xRotationFreeze;
    public bool yRotationFreeze;
    public bool zRotationFreeze;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        if (!xRotationFreeze && !yRotationFreeze && !zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.None;
        }

        if (xRotationFreeze && !yRotationFreeze && !zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.FreezeRotationX;
        }

        if (!xRotationFreeze && yRotationFreeze && !zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.FreezeRotationY;
        }

        if (!xRotationFreeze && !yRotationFreeze && zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.FreezeRotationZ;
        }

        if (xRotationFreeze && yRotationFreeze && !zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
        }

        if (xRotationFreeze && !yRotationFreeze && zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }

        if (!xRotationFreeze && yRotationFreeze && zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }

        if (xRotationFreeze && yRotationFreeze && zRotationFreeze)
        {
            mainBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }
    }
}
