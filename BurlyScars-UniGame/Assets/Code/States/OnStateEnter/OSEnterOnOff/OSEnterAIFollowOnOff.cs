﻿using UnityEngine;
using System.Collections;

public class OSEnterAIFollowOnOff : StateBase
{
	public AIFollow aiFollow;
	public bool onOffCanSearch;
	public bool onOffCanMove;
		
	public override void OnStateEnter (Object sender)
	{
		aiFollow.canSearch = onOffCanSearch;
		aiFollow.canMove = onOffCanMove;
	}
}
