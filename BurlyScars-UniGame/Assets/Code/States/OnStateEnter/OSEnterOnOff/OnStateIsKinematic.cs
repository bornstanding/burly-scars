using UnityEngine;
using System.Collections;

public class OnStateIsKinematic: StateBase 
{
	public Rigidbody bodyToTurnOn;
	
	public override void OnStateEnter(Object sender)
	{
		bodyToTurnOn.isKinematic = true;
	}
	
	public override void OnStateExit(Object sender)
	{
		bodyToTurnOn.isKinematic = false;
	}
}
