using UnityEngine;
using System.Collections;

public class OSEnterAddForce : StateBase
{
	public Vector3 forceSpeed;
	public Rigidbody parentBody;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        parentBody.AddForce(forceSpeed);
    }
}
