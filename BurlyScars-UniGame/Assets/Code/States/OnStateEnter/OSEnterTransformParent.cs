using UnityEngine;
using System.Collections;

public class OSEnterTransformParent : StateBase
{
	public GameObject thingToChild;
	public GameObject parentObject;
	
	public override void OnStateEnter(Object sender)
	{
		if (rootIdentity != null) 
		{
			if(thingToChild)
			{
			thingToChild.transform.parent = parentObject.transform;
			}
		}
	}
	
	public override void OnStateExit(Object sender)
	{
		if (rootIdentity != null)
		{
			if(thingToChild)
			{
				thingToChild.transform.parent = null;
			}
		}
	}
}
