﻿using UnityEngine;
using System.Collections;

public class OSEnterActivate : StateBase 
{
	public IsActivated isActivated;
	
	public override void OnStateEnter(Object sender)
	{
		isActivated.isActivated = true;
	}
}
