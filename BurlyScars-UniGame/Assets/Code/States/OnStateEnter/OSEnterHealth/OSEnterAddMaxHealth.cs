using UnityEngine;
using System.Collections;

public class OSEnterAddMaxHealth : StateBase 
{
	public StandardHealth standardHealth;
	public float addAmount;
	
	public override void OnStateEnter (Object sender)
	{
		standardHealth.maxHealth += addAmount;
	}
}
