﻿using UnityEngine;
using System.Collections;

public class OSEnterHealthMod : StateBase 
{
	public StandardHealth standardHealth;
	public float healthAmount;
	
	public override void OnStateEnter (Object sender)
	{
		if(standardHealth != null)
		{
			standardHealth.currentHealth += healthAmount;
		}
		
		else 
		{
			return;
		}
	}
}
