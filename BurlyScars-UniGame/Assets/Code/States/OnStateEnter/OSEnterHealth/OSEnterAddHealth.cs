using UnityEngine;
using System.Collections;

public class OSEnterAddHealth : StateBase 
{
	public StandardHealth standardHealth;
	public float addAmount;
	
	public override void OnStateEnter (Object sender)
	{
		standardHealth.currentHealth += addAmount;
	}
}
