using UnityEngine;
using System.Collections;

public class OSEnterRestartHealth : StateBase 
{
	public StandardHealth standardHealth;
	
	public override void OnStateEnter (Object sender)
	{
		standardHealth.currentHealth = standardHealth.maxHealth;
	}
}
