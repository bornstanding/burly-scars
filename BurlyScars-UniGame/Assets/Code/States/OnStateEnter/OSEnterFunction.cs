﻿using UnityEngine;
using System.Collections;

public class OSEnterFunction : StateBase 
{
	public string functionName;
		
	public override void OnStateEnter (Object sender) 
	{
		SendMessage(functionName,SendMessageOptions.DontRequireReceiver);
	}
}
