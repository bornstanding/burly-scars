﻿using UnityEngine;
using System.Collections;

public class OSEnterZeroVelocity : StateBase
{
    public Rigidbody mainBody;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);
        mainBody.velocity = new Vector3(0, 0, 0);
    }
}
