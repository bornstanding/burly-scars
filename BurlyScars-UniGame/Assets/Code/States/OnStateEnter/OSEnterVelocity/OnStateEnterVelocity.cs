using UnityEngine;
using System.Collections;

public class OnStateEnterVelocity : StateBase
{
	public Vector3 speed;
	public float xVelocity;
	public GameObject mainGameObject;
	
	public override void OnStateEnter (Object sender)
	{
		xVelocity = mainGameObject.rigidbody.velocity.x;
		mainGameObject.rigidbody.velocity = new Vector3(xVelocity, speed.y, speed.z);
	}
}
