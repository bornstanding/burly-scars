using UnityEngine;
using System.Collections;

public class OnStateEnterShootVelocity : StateBase
{
	public Rigidbody thingToSpawn;
	public GameObject spawnPoint;
	public float speed;
	public Vector3 forceDirection;
		
	public override void OnStateEnter (Object sender)
	{
		Rigidbody clone;
		clone = Instantiate(thingToSpawn, spawnPoint.transform.position, spawnPoint.transform.rotation) as Rigidbody;

		clone.AddForce(transform.TransformDirection(forceDirection * speed));
	}
}
