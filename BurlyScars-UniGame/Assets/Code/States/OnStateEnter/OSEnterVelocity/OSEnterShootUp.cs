﻿using UnityEngine;
using System.Collections;

public class OSEnterShootUp : StateBase
{
    public Rigidbody thingToSpawn;
    public GameObject spawnPoint;
    public float speed;

    public override void OnStateEnter(Object sender)
    {
        Rigidbody clone;
        clone = Instantiate(thingToSpawn, spawnPoint.transform.position, spawnPoint.transform.rotation) as Rigidbody;
        clone.rigidbody.velocity = transform.up * speed;
    }
}
