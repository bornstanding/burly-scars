﻿using UnityEngine;
using System.Collections;

public class OSEnterCurrentVelocity : StateBase 
{
	public OSEnterChangeVelocity changeVelocity;
	public BoostButton boostButton;
	public float newBoostAmount;
	
	public override void OnStateEnter(Object sender)
	{
		boostButton.changeVelocity = changeVelocity;
		boostButton.boostAmount = newBoostAmount;
	}
}
