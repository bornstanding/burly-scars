﻿using UnityEngine;
using System.Collections;

public class OSEnterCounterEvent : StateBase
{
	public FindWithTagAwake findWithTagAwake;
	public float modAmount;

	
	public override void OnStateEnter(Object sender)
	{
		findWithTagAwake.counterEventThree.currentCounter += modAmount;
	}
}
