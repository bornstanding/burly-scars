using UnityEngine;
using System.Collections;

public class TimerChangeState : StateBase
{
	public float timer;
	public float restartTimer;
	public StateManager currentState;
	public StateManager changeStateWhenFinished;
	
	public override void OnStateUpdate (Object sender)
	{
		timer -= 1 * Time.deltaTime;
		if(timer <= 0)
		{
			currentState.ChangeState(changeStateWhenFinished);
			timer = restartTimer;
		}
	}

	public override void OnStateExit (Object sender)
	{
		timer = restartTimer;
	}
}
