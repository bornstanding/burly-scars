using UnityEngine;
using System.Collections;

public class ParentChangeToOneState : MonoBehaviour 
{
	public StateManager currentState;
	public StateManager enterTriggerState;
	public StateManager exitTriggerState;
	public string tagged;
	
	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			currentState.ChangeState(enterTriggerState);
		}
	}
	
	void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			enterTriggerState.ChangeState(exitTriggerState);
		}
	}
}
