﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeCurrentMovement : StateBase
{
    public SideScrollMovement sideScrollMovemnt;
    public float newMoveSpeed;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);
        sideScrollMovemnt.moveSpeed = newMoveSpeed;
    }
}
