﻿using UnityEngine;
using System.Collections;

public class OSEnterChangeMovement : StateBase
{
    public SideScrollMovement sideScrollMovement;
    public float newMaxSpeed;
    public float newAcceleration;
    public float newFriction;

    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);

        sideScrollMovement.maxSpeed = newMaxSpeed;
        sideScrollMovement.acceleration = newAcceleration;
        sideScrollMovement.friction = newFriction;
    }
}
