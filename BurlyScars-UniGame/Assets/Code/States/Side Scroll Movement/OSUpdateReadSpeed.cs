﻿using UnityEngine;
using System.Collections;

public class OSUpdateReadSpeed : MonoBehaviour
{
	public SideScrollMovement sideScrollMovement;
	public Rigidbody mainBody;
	public float speed;

	void Awake()
	{
		sideScrollMovement = GameObject.FindObjectOfType(typeof(SideScrollMovement)) as SideScrollMovement;
	}

	void Update()
	{
		speed = sideScrollMovement.moveSpeed;

		mainBody.velocity = (transform.TransformDirection(Vector3.left*speed));

		if(Input.GetKey(sideScrollMovement.moveLeftButton))
		{
			mainBody.transform.eulerAngles = new Vector3(0, 0, 0);
		}

		if(Input.GetKey(sideScrollMovement.moveRightButton))
		{
			mainBody.transform.eulerAngles = new Vector3(0, 180, 0);
		}
	}
}
