﻿using UnityEngine;
using System.Collections;

public class OSEnterAIChangeTarget : StateBase 
{
	public AIFollow aiFollow;
    public FindGameWithTagAwake gameObjectAwake;
	
	/*void Awake()
	{
		newTarget = GameObject.FindWithTag(tagged);
	}*/
	
	public override void OnStateEnter (Object sender)
	{
	    if (gameObjectAwake != null)
	    {
	        aiFollow.target = gameObjectAwake.objectWithTag.transform;
	    }

	    else
	    {
	        aiFollow.target = null;
	    }
	}
		
}
