using UnityEngine;
using System.Collections;

public class OnStateExitActivations : StateBase
{
	public IsActivated isActivated;
	
	public override void OnStateEnter (Object sender)
	{
		isActivated.isActivated = true;
	}
	
	public override void OnStateExit (Object sender)
	{
		isActivated.isActivated = false;
	}
}
