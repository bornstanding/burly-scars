﻿using UnityEngine;
using System.Collections;

public class OSUpdateTimer : StateBase
{
    public float timer;
    public float timerRate;
    public float maxTimer;

    public event EventHandlers.MinimalHandler TimerComplete = delegate { };

    public override void OnStateUpdate(Object sender)
    {
        base.OnStateUpdate(sender);

        timer += timerRate*Time.deltaTime;

        if (timer >= maxTimer)
        {
            timer = maxTimer;
            TimerComplete();
        }
    }
}
