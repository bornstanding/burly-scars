﻿using UnityEngine;
using System.Collections;

public class OSUpdateCheckThingToChild : StateBase
{
	public OSEnterTransformParent transformParent;
	public StateManager currentState;
	public StateManager nextState;
	
	public override void OnStateUpdate (Object sender)
	{
		if(transformParent.thingToChild == null)
		{
			print("no children!");
			currentState.ChangeState(nextState);
		}
	}
}
