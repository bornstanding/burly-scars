﻿using UnityEngine;
using System.Collections;

public class DoubleJumpCalc : StateBase 
{
	public OSEnterAddForce oSEnterAddForce;
	public float minJumpForce;
	public float forceCalc;
	public float maxJumpForce;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		oSEnterAddForce.forceSpeed.y = minJumpForce;
	}

	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);

		oSEnterAddForce.forceSpeed.y += (forceCalc*Time.deltaTime);

		if(oSEnterAddForce.forceSpeed.y >= maxJumpForce)
		{
			oSEnterAddForce.forceSpeed.y = maxJumpForce;
		}
	}
}
