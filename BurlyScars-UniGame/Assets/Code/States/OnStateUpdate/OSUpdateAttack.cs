﻿using UnityEngine;
using System.Collections;

public class OSUpdateAttack : StateBase
{
	public DistanceBetween2Objects distance;
	public float lookSpeed;
	public float multiLookSpeed;
	public float minLookSpeed;
	public Transform mainTransform;
	public float moveSpeed;
	public Rigidbody mainBody;
	
	public override void OnStateUpdate(Object sender)
	{
        // Look directly at the player
		Vector3 lookDir = (distance.character.transform.position - mainTransform.position);
		// Take velocity into account
		lookDir += distance.character.rigidbody.velocity;
		
		lookDir.y = 0;
		lookDir.Normalize();
		Quaternion targetRotation = Quaternion.LookRotation(lookDir, Vector3.up);
		
		mainTransform.rotation = Quaternion.RotateTowards(mainTransform.rotation, targetRotation, lookSpeed * Time.smoothDeltaTime);
		
		//make sure it only rotates on it's Y axis
		mainTransform.eulerAngles = new Vector3(0, mainTransform.eulerAngles.y, 0);
		
		//add velocity forward
		mainBody.velocity = transform.forward * moveSpeed;
		
		lookSpeed = ((distance.distanceBetween * multiLookSpeed) + minLookSpeed);
	}
}
