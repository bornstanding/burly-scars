﻿using UnityEngine;
using System.Collections;

public class OSUpdateAddForceForward : StateBase
{
    public float speed;
    public Rigidbody mainBody;
    public Vector3 forceDirection;

    public override void OnStateUpdate(Object sender)
    {
        mainBody.AddForce(transform.TransformDirection(forceDirection * speed));
    }
}
