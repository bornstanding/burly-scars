﻿using UnityEngine;
using System.Collections;

public class OSUpdateModHealthOverTime : StateBase 
{
	public StandardHealth standardHealth;
	public float healthModOverTime;
	
	public override void OnStateUpdate (Object sender)
	{
		standardHealth.currentHealth += healthModOverTime * Time.deltaTime;
	}
}
