﻿using UnityEngine;
using System.Collections;

public class OSUpdateVelocityForward : StateBase
{
    public float speed;
	public Rigidbody mainBody;
    public Vector3 velocityDirection;
	
	public override void OnStateUpdate (Object sender)
	{
	    mainBody.velocity = transform.TransformDirection(velocityDirection*speed);
	}
}
