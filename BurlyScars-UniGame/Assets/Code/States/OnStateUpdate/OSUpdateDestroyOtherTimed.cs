using UnityEngine;
using System.Collections;

public class OSUpdateDestroyOtherTimed : StateBase 
{
	public GameObject otherObject;
	public float timer;
	
	public override void OnStateUpdate (Object sender)
	{
		timer -= 1 * Time.deltaTime;
		if(timer <= 0)
		{
		    Destroy(otherObject);
		}
	}
}
