﻿using UnityEngine;
using System.Collections;

public class OSUpdateLerpScale : StateBase
{
    public GameObject objectToScale;
    public float lerpTimer;
    public Vector3 lerpTowards;


    public override void OnStateFixedUpdate(Object sender)
    {
        base.OnStateUpdate(sender);

        objectToScale.transform.localScale = Vector3.Lerp(objectToScale.transform.localScale, lerpTowards, lerpTimer * Time.deltaTime);
        

        //Vector3 roundedScale = objectToScale.transform.localScale / objectToScale.transform.localScale;

        //roundedScale = Vector3.Lerp(roundedScale, lerpTowards, lerpTimer*Time.deltaTime);
    }
}
