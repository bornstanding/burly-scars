﻿using UnityEngine;
using System.Collections;

public class OSUpdateLookAt : StateBase
{
    public FindGameWithTagAwake gameObjectWithTag;
    public Transform thingLooking;
    public float speed;
    public string tagged;

    public override void OnStateUpdate(Object sender)
    {
        base.OnStateUpdate(sender);

        thingLooking.LookAt(gameObjectWithTag.objectWithTag.transform);

        thingLooking.position = Vector3.MoveTowards(thingLooking.position, gameObjectWithTag.objectWithTag.transform.position, speed*Time.deltaTime);
    }
}
