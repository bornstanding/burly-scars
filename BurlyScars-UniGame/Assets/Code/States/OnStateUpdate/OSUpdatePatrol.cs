﻿using UnityEngine;
using System.Collections;

public class OSUpdatePatrol : StateBase
{
    public AIFollow Ai;
    public float MaxNumber;
    public float MinNumber;
    public int MaxRandomNumber;
    public override void OnStateEnter(Object sender)
    {
        base.OnStateEnter(sender);
        Ai = rootIdentity.GetComponent<AIFollow>();
    }
    public override void OnStateUpdate(Object sender)
    {
        base.OnStateUpdate(sender);
        if (Random.Range(0,MaxRandomNumber)==0)
        {
            Ai.targetPosition = MathsHelper.RandomVector3(MinNumber, MaxNumber);
        }
    }
}
