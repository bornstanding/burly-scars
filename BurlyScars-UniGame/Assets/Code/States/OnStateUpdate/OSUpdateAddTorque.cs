﻿using UnityEngine;
using System.Collections;

public class OSUpdateAddTorque : StateBase
{
    public Rigidbody mainBody;
    public Vector3 torqueAmount;

    public override void OnStateUpdate(Object sender)
    {
        base.OnStateEnter(sender);

        mainBody.AddTorque(torqueAmount);
    }
}
