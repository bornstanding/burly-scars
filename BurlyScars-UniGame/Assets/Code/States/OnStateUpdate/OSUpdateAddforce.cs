﻿using UnityEngine;
using System.Collections;

public class OSUpdateAddforce : StateBase
{
	public Vector3 forceSpeed;
	public Rigidbody parentBody;
	
	public override void OnStateFixedUpdate(Object sender)
	{
		base.OnStateFixedUpdate(sender);
		
		parentBody.AddForce(forceSpeed);
	}
}
