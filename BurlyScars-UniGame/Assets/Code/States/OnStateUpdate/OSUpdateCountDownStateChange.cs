﻿using UnityEngine;
using System.Collections;

public class OSUpdateCountDownStateChange : StateBase
{
    public StateManager currentState;
    public StateManager nextState;
    public float timer;

    public override void OnStateUpdate(Object sender)
    {
        base.OnStateEnter(sender);

        timer -= 1*Time.deltaTime;
        if (timer <= 0)
        {
            currentState.ChangeState(nextState);
        }
    }
}
