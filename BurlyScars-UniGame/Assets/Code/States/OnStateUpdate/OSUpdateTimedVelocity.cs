﻿using UnityEngine;
using System.Collections;

public class OSUpdateTimedVelocity : StateBase
{
	public float speed;
	public Rigidbody thingToSpawn;
	public GameObject spawnPoint;
	public float timer;
	public float restartTimer;
	
	public override void OnStateUpdate (Object sender)
	{
		timer -= 1 * Time.deltaTime;
		if(timer <= 0)
		{
		    Rigidbody clone;
		    clone = Instantiate(thingToSpawn, spawnPoint.transform.position, spawnPoint.transform.rotation) as Rigidbody;
		    clone.rigidbody.velocity = transform.forward * speed;
			timer = restartTimer;
		}
	}
}
