using UnityEngine;
using System.Collections;

public class SelectLevelButton : MonoBehaviour 
{
	public string levelName;
	
	void OnClick()
	{
		print("Start");
		Application.LoadLevel(levelName);
		Time.timeScale = 1;
	}
}
