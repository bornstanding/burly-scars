using UnityEngine;
using System.Collections;

public class OnClickChangeState : MonoBehaviour
{
    public StateManager currentState;
    public StateManager nextState;

	void OnClick()
	{
		currentState.ChangeState(nextState);
	}
}