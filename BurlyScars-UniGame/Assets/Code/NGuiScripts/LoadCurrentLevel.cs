using UnityEngine;
using System.Collections;

public class LoadCurrentLevel : MonoBehaviour 
{
	void OnClick()
	{
		Time.timeScale = 1;
		Application.LoadLevel(Application.loadedLevel);
	}
}
