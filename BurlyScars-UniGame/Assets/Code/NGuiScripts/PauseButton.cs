using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour 
{
	public GameObject pauseMenu;
	
	public event EventHandlers.MinimalHandler PauseActivated = delegate { };
	
	void OnClick()
	{
		Time.timeScale = 0;
		pauseMenu.SetActive(true);
		PauseActivated();
	}
}
