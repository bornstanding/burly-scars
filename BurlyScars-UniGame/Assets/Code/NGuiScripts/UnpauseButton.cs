using UnityEngine;
using System.Collections;

public class UnpauseButton : MonoBehaviour 
{
	public GameObject pauseMenu;
	
	public event EventHandlers.MinimalHandler UnPausedActivated = delegate { };
	
	void OnClick()
	{
		Time.timeScale = 1;
		pauseMenu.SetActive(false);
		UnPausedActivated();
	}
}
