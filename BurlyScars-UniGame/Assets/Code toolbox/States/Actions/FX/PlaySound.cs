using UnityEngine;

internal class PlaySound : StateBase
{
	public bool AutoFindAudioSource = true;
	public AudioSource mainAudioSource;
	public AudioClip clip = null;

	public int startOffsetSamples = 0;
	public float pitch = 1;

	public override void Awake()
	{
		base.Awake();

		if(AutoFindAudioSource)
			mainAudioSource = rootIdentity.GetComponentInChildren<AudioSource>();
	}

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		mainAudioSource.clip = clip;
		
		if (mainAudioSource != null)
		{
			mainAudioSource.Play();
			//mainAudioSource.PlayOneShot(clip);
			mainAudioSource.timeSamples = startOffsetSamples;
			mainAudioSource.pitch = pitch;
		}
		else
			Debug.LogWarning("No AudioSource found!", this);
	}
}
