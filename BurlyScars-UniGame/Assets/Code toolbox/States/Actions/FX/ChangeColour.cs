using UnityEngine;

public class ChangeColour : StateBase
{
	public Color colour;
	public Renderer mainRenderer;

	public override void Awake()
	{
		base.Awake();

		// Look for the renderer component by started at the highest GO and searching inside all the children
		//mainRenderer = rootIdentity.GetComponentInChildren<Renderer>();
	}

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		mainRenderer.material.color = colour;
	}
}
