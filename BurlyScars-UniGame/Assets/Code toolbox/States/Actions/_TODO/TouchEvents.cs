using UnityEngine;

public class TouchEvents : StateBase
{
	public StateManager OnTouchBegan;
	public StateManager OnTouchEnded;
	public StateManager OnMoved;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		TouchControllerEvents.OnTouchControllerBegan += TouchControllerEvents_OnTouchControllerBegan;
		TouchControllerEvents.OnTouchControllerEnded += TouchControllerEvents_OnTouchControllerEnded;
		TouchControllerEvents.OnTouchControllerMoved += TouchControllerEvents_OnTouchControllerMoved;
	}

	public override void OnStateExit(Object sender)
	{
		base.OnStateExit(sender);

		TouchControllerEvents.OnTouchControllerBegan -= TouchControllerEvents_OnTouchControllerBegan;
		TouchControllerEvents.OnTouchControllerEnded -= TouchControllerEvents_OnTouchControllerEnded;
		TouchControllerEvents.OnTouchControllerMoved -= TouchControllerEvents_OnTouchControllerMoved;
	}

	void TouchControllerEvents_OnTouchControllerMoved(TouchContainer touch)
	{
		if(OnMoved != null)
			stateManager.ChangeState(OnMoved);
	}

	void TouchControllerEvents_OnTouchControllerBegan(TouchContainer touch)
	{
		if(OnTouchBegan != null)
			stateManager.ChangeState(OnTouchBegan);
	}

	void TouchControllerEvents_OnTouchControllerEnded(TouchContainer touch)
	{
		if(OnTouchEnded != null)
			stateManager.ChangeState(OnTouchEnded);
	}
}
