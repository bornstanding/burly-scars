using UnityEngine;

public class PlayMusic : StateBase
{
	public delegate void Play(AudioClip clip, float startTime);

	public static event Play PlayEvent = delegate { };

	public AudioClip music;
	public float currentPlayTime;
	private float startTime;

	public void Start()
	{
		startTime = Time.timeSinceLevelLoad;
	}

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		PlayEvent(music, currentPlayTime);

//		MusicManager.Instance.PlayMusic(music);
/*
		if(!audio.isPlaying)
		{
			//audio.clip = music;
			//audio.Play();
			//audio.time = currentPlayTime;
		}
*/
	}

	public override void OnStateExit(Object sender)
	{
		base.OnStateExit(sender);

		currentPlayTime = Time.timeSinceLevelLoad - startTime;
	}
}