using UnityEngine;

public class SetPhysics : StateBase
{
	public bool findNearestRigidbody = true;
	public Rigidbody useRigidbody;
	public bool setDrag = true;
	public float drag;
	public bool setAngularDrag = false;
	public float angularDrag;
	public Vector3 moveDirection;
	public float forceScalar = 100;
	public float rotateSpeed;

	public override void Awake()
	{
		base.Awake();

		if(findNearestRigidbody)
			useRigidbody = ToolBox.GetComponentInParents<Rigidbody>(transform);
	}

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		if(useRigidbody != null)
		{
			if(setDrag)
				useRigidbody.drag = drag;
			if(setAngularDrag)
				useRigidbody.angularDrag = angularDrag;
		}
	}

/*
	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);
	}
*/

	public void Move(Vector2 _moveDirection)
	{
		moveDirection.x = _moveDirection.x;
		moveDirection.z = _moveDirection.y;
		useRigidbody.transform.forward = Vector3.Lerp(useRigidbody.transform.forward, Vector3.Normalize(new Vector3(_moveDirection.x, 0, _moveDirection.y)), rotateSpeed);
		useRigidbody.AddForce((moveDirection*forceScalar));
	}
}
