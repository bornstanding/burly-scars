using UnityEngine;

public class ChangeState : StateBase
{
	public float delay;
	public bool autorun = true;

	public StateManager changeToState;

	// Called late to avoid skipping other components running on StateEnter
	public override void OnStateEnterLate(Object sender)
	{
		base.OnStateEnterLate(sender);

		if(autorun)
			TriggerTransitionToNextState();
	}

	// Seperate function so things like ZZSigSlot can call it if it wants to
	public void TriggerTransitionToNextState()
	{
		if(changeToState != null)
			stateManager.ChangeState(changeToState, delay);
		else
			stateManager.NextState(delay);
	}
}
