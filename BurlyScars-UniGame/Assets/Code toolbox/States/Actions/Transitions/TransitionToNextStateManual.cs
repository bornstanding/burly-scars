using UnityEngine;

public class TransitionToNextStateManual : StateBase
{
	public float delay;
	public bool autorun = true;
	
	// Called late to avoid skipping other components running on StateEnter
	public override void OnStateEnterLate(Object sender)
	{
		base.OnStateEnterLate(sender);

		Debug.LogWarning("This component has been replaced by 'ChangeState.cs'", this);

		if(autorun)
			TransitionToNextState();
	}

	// Seperate function so things like ZZSigSlot can call it if it wants to
	public void TransitionToNextState()
	{
		stateManager.NextState(delay);
	}
}
