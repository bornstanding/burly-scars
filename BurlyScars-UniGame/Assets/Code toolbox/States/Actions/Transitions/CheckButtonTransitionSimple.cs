using UnityEngine;
using System.Collections;

public class CheckButtonTransitionSimple : StateBase
{
	public KeyCode key;
	public StateManager nextState;

	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);

		if(Input.GetKey(key))
		{
			stateManager.ChangeState(nextState);
		}
		
		else
		{
			return;
		}
	}
}
