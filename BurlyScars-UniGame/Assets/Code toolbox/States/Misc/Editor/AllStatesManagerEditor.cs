﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AllStatesManager))]
public class AllStatesManagerEditor : Editor
{
	static GUISkin titles;
	static GUISkin active;
	static GUISkin inActive;
	static GUISkin groups;
	AllStatesManager allStatesManager;

	public override void OnInspectorGUI()
	{
		// Check to see if these are initialised and do it only once. CHECK: I don't know how to do constructor type things in the GUI?);
		if(!allStatesManager)
		{
			// Manually load skins because you can't drag drop them in the inspector
			titles =
				(GUISkin)Resources.Load("Skins/Titles", typeof(GUISkin));
			active =
				(GUISkin)Resources.Load("Skins/Active", typeof(GUISkin));
			inActive =
				(GUISkin)Resources.Load("Skins/InActive", typeof(GUISkin));
			groups =
				(GUISkin)Resources.Load("Skins/Groups", typeof(GUISkin));

			allStatesManager = (AllStatesManager)target;
			if(!allStatesManager.gameObject)
				return;
		}

		//DrawDefaultInspector();
		//EditorGUIUtility.LookLikeControls();

		// Bail if not in play mode. TODO: I could make this work here
		if (!EditorApplication.isPlaying)
		{
			GUILayout.Label("This only displays all states while playmode is active");
			return;
		}

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();
		// **************************************
		//	Current States
		// **************************************
//		GUI.skin = titles;
//		GUILayout.Label("Current states");
//		GUI.skin = active;
//		foreach (SingleStateManager allStatesManager in allStatesManager.currentStates)
//			if (DrawButtons(allStatesManager)) break;
//
//		EditorGUILayout.Separator();
//		EditorGUILayout.Separator();

		// **************************************
		//	All States
		// **************************************
		GUI.skin = titles;
		GUILayout.Label("All states");

		GUI.skin = active;
		if(GUILayout.Button("Exit all"))
		{
			allStatesManager.ExitAllRunningStates();
			EditorUtility.SetDirty(allStatesManager.gameObject); //this updates Inspector GUI
//			GameObject hackGO = new GameObject("Hack to force refresh");
//			Destroy(hackGO);
		}

		// **************************************
		//	Individual States
		// **************************************

		// Scan all StateGroups (including the statemanager for non-grouped states, hence the 'parent' below)
		foreach(StateGroup stateGroup in allStatesManager.transform.parent.GetComponentsInChildren<StateGroup>())
		{
			GUI.skin = groups;
			GUILayout.Label("Group: " + stateGroup.name);

			foreach(StateManager state in allStatesManager.allStates)
				if(state.groupParent == stateGroup)
					DrawButtons(state, true, true);
		}

		if(GUI.changed)
			EditorUtility.SetDirty(allStatesManager);
	}

	public static void DrawButtons(StateManager singleStateManager, bool drawLabel, bool drawInitialState)
	{
		float width = 45;

		GUILayout.BeginHorizontal();
		if(singleStateManager.gameObject.activeSelf)
			GUI.skin = active;
		else
			GUI.skin = inActive;

		if(drawLabel)
			GUILayout.Label(singleStateManager.name, GUILayout.Width(width*2));

		if(GUILayout.Button("Next", GUILayout.Width(width)))
			if(singleStateManager.gameObject.activeSelf)
				singleStateManager.NextState();
//			return true;

		// This button is always active. It allows you to force enter a state to test stability of the states
		GUI.skin = active;
		if(GUILayout.Button("Enter", GUILayout.Width(width)))
			singleStateManager.ChangeState(singleStateManager);
		// Change state to itself. Even if it's the same as the current state
//			return true;

		if (singleStateManager.gameObject.activeSelf)
			GUI.skin = active;
		else
			GUI.skin = inActive;

		if (GUILayout.Button("Exit", GUILayout.Width(width)))
			singleStateManager.ExitState();
//			return true;

		// Initial state?
		if(drawInitialState)
			GUILayout.Toggle(singleStateManager.initialState, "");
//			return false;

		GUILayout.EndHorizontal();
//		GUILayout.EndHorizontal();
		GUI.skin = null;
//		return false;
	}
}
