using System;
using UnityEngine;

public class TriggerStayEvent : MonoBehaviour
{
	public delegate void OnTriggerStayDelegate();
	public event OnTriggerStayDelegate OnTriggerStayEvent = delegate { };

	public void OnTriggerStay(Collider other)
	{
		OnTriggerStayEvent();
	}
}
