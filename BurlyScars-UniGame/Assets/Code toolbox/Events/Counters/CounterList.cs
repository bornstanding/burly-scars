using System.Collections.Generic;
using UnityEngine;

public class CounterList : MonoBehaviour
{
	// Use ZZ to trigger this
	// This needs the event to send along it's GameObject within the event (useful for static events where there's no direct GameObject drag-dropping)
	// We need to give a list of GameObjects it's interested in.
	// TODO: Doesn't work for IN GAME spawned objects yet. Because it needs a drag-dropped list of GO's to check.

	// Variables and events
	public List<GameObject> objectsToListenTo;

	public delegate void Simple();
	public event Simple CounterFinishedEvent = delegate { };


	// Functions
	public void MinusOne(GameObject sender)
	{
		if(objectsToListenTo.Count < 1)
			return;

		// Here instead of a simple 'int' counter variable, 
		// we remove actual GameObject references from a list, until it's empty.
		objectsToListenTo.Remove(sender);

		if(objectsToListenTo.Count == 0)
		{
			CounterFinishedEvent();
		}
	}
}
