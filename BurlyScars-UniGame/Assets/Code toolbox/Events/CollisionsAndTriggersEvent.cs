using UnityEngine;

public class CollisionsAndTriggersEvent : MonoBehaviour
{
	public event EventHandlers.MinimalHandler OnTriggerStayEvent = delegate { };
	public event EventHandlers.MinimalHandler OnTriggerExitEvent = delegate { };
	public event EventHandlers.MinimalHandler OnTriggerEnterEvent = delegate { };
	public event EventHandlers.MinimalHandler OnCollisionEnterEvent = delegate { };
	public event EventHandlers.MinimalHandler OnCollisionStayEvent = delegate { };
	public event EventHandlers.MinimalHandler OnCollisionExitEvent = delegate { };

	public void OnCollisionEnter(Collision other) 
	{ 
		OnCollisionEnterEvent(); 	
	}
	
	public void OnCollisionStay(Collision other) 
	{ 
		OnCollisionStayEvent(); 
	}
	
	public void OnCollisionExit(Collision other) 
	{ 
		OnCollisionExitEvent(); 	
	}
	
	public void OnTriggerEnter(Collider other) 
	{ 
		OnTriggerEnterEvent(); 	
	}
	
	public void OnTriggerStay(Collider other) 
	{ 
		OnTriggerStayEvent(); 	
	}
	
	public void OnTriggerExit(Collider other) 
	{ 
		OnTriggerExitEvent(); 	
	}
}
