using UnityEngine;

public class GenericEvent : MonoBehaviour
{
	public event EventHandlers.MinimalHandler Generic = delegate { };

	public void FireEvent()
	{
		Generic();
	}
}
