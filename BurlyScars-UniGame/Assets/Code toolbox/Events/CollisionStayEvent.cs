using UnityEngine;

public class CollisionStayEvent : MonoBehaviour
{
	public event EventHandlers.MinimalHandler OnCollisionStayEvent = delegate { };

	public void OnCollisionStay(Collision other)
	{
		OnCollisionStayEvent();
	}
}
