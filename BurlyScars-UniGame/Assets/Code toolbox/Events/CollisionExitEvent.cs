using UnityEngine;

public class CollisionExitEvent : MonoBehaviour
{
	public event EventHandlers.MinimalHandler OnCollisionExitEvent = delegate { };

	public void OnCollisionExit()
	{
		OnCollisionExitEvent();
	}
}
