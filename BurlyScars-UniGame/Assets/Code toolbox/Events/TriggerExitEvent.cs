using UnityEngine;

public class TriggerExitEvent : MonoBehaviour
{
	public delegate void OnTriggerExitDelegate();
	public event OnTriggerExitDelegate OnTriggerExitEvent = delegate { };

	public void OnTriggerExit(Collider other)
	{
		OnTriggerExitEvent();
	}
}
