using UnityEngine;

public class Button : MonoBehaviour
{
	// Simple event
	public delegate void Simple();

	public delegate void WithSender(GameObject sender);

	public event Simple Activate = delegate { };
	public static event WithSender ActivateStaticEvent = delegate { };

	public bool allowMultiplePresses;
	public bool triggered;
	public bool sendLocalEvents = true;
	public bool sendStaticEvents = false;

	public void OnTriggerEnter()
	{
		// This can be hit many times, so fire event everytime, then bail ('return')
		if(allowMultiplePresses)
			FireActivateEvents();
		else if(triggered == false) // This is a one shot button. Check if it's been triggered yet. DON'T fire event if it HAS been though.
			FireActivateEvents();
	}

	void FireActivateEvents()
	{
		if(sendLocalEvents)
			Activate();
		if(sendStaticEvents)
			ActivateStaticEvent(gameObject);
	}
}
