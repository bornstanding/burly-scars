using UnityEngine;

public class CharacterController : MonoBehaviour
{
	public float speed = 100;

	public void Update()
	{
		Vector3 direction = new Vector3(Input.GetAxis("Horizontal")*speed, 0, Input.GetAxis("Vertical")*speed);
		direction *= Time.deltaTime;
		rigidbody.AddForce(direction);

		Vector3 velocity = rigidbody.velocity;
		velocity.y = 0; // Don't want him to look up and down
		if(velocity != Vector3.zero)
			transform.forward = velocity;
	}
}
