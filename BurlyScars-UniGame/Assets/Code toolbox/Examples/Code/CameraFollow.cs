using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	public GameObject target;
	public Vector3 offset;
	public string followTagger;

	public void Update()
	{
		target = GameObject.FindGameObjectWithTag(followTagger);

		if(target != null)
		{
			transform.position = target.transform.position + offset;
		}

        else
        {
            return;
        }
	}
}
