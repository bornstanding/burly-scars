using UnityEngine;
using System.Collections;

public class Jumper : MonoBehaviour
{
	public float height;

	// Update is called about 60 times a second (once per frame)
	public void Jump()
	{
		rigidbody.AddForce(0, height, 0, ForceMode.Impulse);
	}

	public void Thrust()
	{
		rigidbody.AddForce(0,0,height, ForceMode.Impulse);
	}
}
