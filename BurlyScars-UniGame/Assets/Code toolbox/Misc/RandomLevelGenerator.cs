using System;
using System.Collections;
using LibNoise.Unity.Generator;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class RandomVector3Range
{
	public Vector3 min;
	public Vector3 max;
}

public class RandomLevelGenerator : MonoBehaviour
{
	public GameObject[] thingsToSpawn;

	public Transform parent;
	public string rootName = "Generated Level";
	GameObject rootGO;

	public bool usePerlinNoise;
	public MathsHelper.Range spawnPerlinThreshold;
	public Vector3 perlinDensity;

	public Vector3 minimumSpacedAmount;
	public Vector3 startPosition;
	public Vector3 perlinStartPosition;
	public int totalPieces;

	public bool centerPieces;
	public Vector3 numberOfPiecesScalar;

	public MathsHelper.Range rotationRange;
	public RandomVector3Range randomPosition;
	public RandomVector3Range randomScale;
	public Vector3 minimumScale;
	public int randomSeed;

	public bool DebugRegenerateContinuously;
	public float DebugRegenerateFrequency = 1;

	public Perlin perlin;

	Quaternion rot;
	GameObject go;
	public bool addToStatic;

	public bool supportTidyTileMapper;
	public GameObject tidyTileHelperMessageReceiver;

	// Use this for initialization
	// Network started Message receiver (Use even for single player as everything is 'networked')
//	public void OnNetworkLoadedLevel()

	public void Start()
	{
		/*
		Debug.Log("Level loader : isServer = " + Network.isServer);
		if (Network.isServer)
		{
		}
		*/
		perlin = new Perlin();

		if(DebugRegenerateContinuously)
			StartCoroutine(GenerateLevelContinuous());
		else
			GenerateLevel();
	}

	public IEnumerator GenerateLevelContinuous()
	{
		while(true)
		{
			GenerateLevel();

			yield return new WaitForSeconds(DebugRegenerateFrequency);
		}
	}

	public void GenerateLevel()
	{
		//StopAllCoroutines(); // CHECK the coroutines for states bugs out on the balls
		for(int i = 0; i < parent.transform.GetChildCount(); i++)
			Destroy(parent.transform.GetChild(i).gameObject);

		rootGO = new GameObject(rootName);
		rootGO.transform.parent = parent.transform;

		if(usePerlinNoise)
			Perlin();
		else
			RandomGenerator();

		if(supportTidyTileMapper && tidyTileHelperMessageReceiver)
		{
			tidyTileHelperMessageReceiver.SendMessage("FinishModifyMap"); // This is a message so you don't need TidyTileMapper in your project
		}

	}

	void Perlin()
	{
		Vector3 pos;
		float perlinNoise;
		//GameObject go;
		Vector3 spaced;
		//Quaternion rot = new Quaternion();

		Vector3 start;
		Vector3 end;

		// Spawn AROUND the start position
		if(centerPieces)
		{
			start = -numberOfPiecesScalar/2;
			end = numberOfPiecesScalar/2;
		}
		else // Spawn FROM the start position
		{
			start = Vector3.zero;
			end = numberOfPiecesScalar;
		}

		for(int x = (int)start.x; x < end.x; x++)
			for(int y = (int)start.y; y < end.y; y++)
				for(int z = (int)start.z; z < end.z; z++)
				{
					spaced.x = x*minimumSpacedAmount.x;
					spaced.y = y*minimumSpacedAmount.y;
					spaced.z = z*minimumSpacedAmount.z;

					pos = startPosition + spaced;

					//				float perlinNoise = Mathf.PerlinNoise(pos.x*perlinDensity.x, pos.y*perlinDensity.y);
					perlinNoise = (float)perlin.GetValue(perlinStartPosition + MathsHelper.MultiplyVector3(pos, perlinDensity));

					//Debug.Log(perlinNoise+ " : pos = "+pos);
					if(perlinNoise >= spawnPerlinThreshold.min && perlinNoise < spawnPerlinThreshold.max)
						SpawnObject(pos);
				}

		if(addToStatic)
			StaticBatchingUtility.Combine(rootGO);

		return;

/*
		float x = 0.1f,y = 0.1f;
		while (x < 50)
		{
			float xPerlin = Mathf.PerlinNoise((float)x / 5.1f, 0) * 4;
			x = x + 1+xPerlin;

			y = 0;

			while (y < 50)
			{
				float yPerlin = Mathf.PerlinNoise(0, (float)y / 5.1f)*4;
				y = y + 1+yPerlin;

				Vector3 pos = startPosition;
				GameObject go = (GameObject)Instantiate(thingsToSpawn[Random.Range(0, thingsToSpawn.Length)], pos, Quaternion.identity);
				go.transform.localScale = new Vector3(xPerlin, yPerlin, 3 + Random.Range(-0.02f, 0.02f));

			}

		}
*/
	}

	void SpawnObject(Vector3 pos)
	{
		print("Block = " + pos);
		if(!supportTidyTileMapper && thingsToSpawn.Length > 0)
		{
			rot = Quaternion.Euler(0, 0, Random.Range(rotationRange.min, rotationRange.max));
			go = Instantiate(thingsToSpawn[Random.Range(0, thingsToSpawn.Length)], pos, rot) as GameObject;
			go.transform.parent = rootGO.transform;
			//go.isStatic = true;
		}
		else if(supportTidyTileMapper && tidyTileHelperMessageReceiver)
		{
			tidyTileHelperMessageReceiver.SendMessage("ModifyMap", pos); // This is a message so you don't need TidyTileMapper in your project
		}
	}

	void RandomGenerator()
	{
		for(int x = 0; x < numberOfPiecesScalar.x; x++)
			for(int y = 0; y < numberOfPiecesScalar.y; y++)
			{
				Vector3 pos = startPosition + new Vector3((x*minimumSpacedAmount.x) + Random.Range(randomPosition.min.x, randomPosition.max.x), (y*minimumSpacedAmount.y) + Random.Range(randomPosition.max.y, randomPosition.max.y), 0);
				Quaternion rot = new Quaternion();
				rot = Quaternion.Euler(0, 0, Random.Range(rotationRange.min, rotationRange.max));
				//GameObject go = (GameObject)Network.Instantiate(thingsToSpawn[Random.Range(0, thingsToSpawn.Length)], pos, Quaternion.identity, 0);
				GameObject go = (GameObject)Instantiate(thingsToSpawn[Random.Range(0, thingsToSpawn.Length)], pos, rot);
//					go.transform.localScale = new Vector3(Random.Range(-randomScale, randomScale), Random.Range(-randomScale, randomScale), 4 + Random.Range(-0.02f, 0.02f));
				go.transform.localScale = minimumScale + new Vector3(Random.Range(randomScale.min.x, randomScale.max.x), Random.Range(randomScale.min.y, randomScale.max.y), Random.Range(randomScale.min.z, randomScale.max.z));
//				MeshRenderer mesh = go.GetComponentInChildren<MeshRenderer>();
//					go.GetComponent<NetworkedLevelObject>().Colour = new Color(Random.value, Random.value, Random.value);

				//		    Animation anim = go.GetComponentInChildren<Animation>();
				//		    if (anim != null)
				//		    {
				//		        anim.Stop();
				//		        anim.Play();
				//                Invoke("StartMoving", Random.value*10.0f);
				//		    }
			}
	}
}
