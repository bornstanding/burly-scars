﻿using UnityEngine;

public class DebugManager : MonoBehaviour
{
	/*
	private static DebugManager instance = null;
	public static DebugManager Instance
	{
		get
		{
			if (instance == null)
			{
				// Maybe it was manually placed in the scene
				instance = FindObjectOfType(typeof(DebugManager)) as DebugManager;

				// Nope, so create one
				if(instance == null)
				{
					GameObject go = new GameObject();
					instance = go.AddComponent<DebugManager>();
					go.name = "DebugManager singleton";
				}
			}

			return instance;
		}
	}
*/
	public static int debugLevel = 1;
	public static bool showWarnings = true;

//	public int debugLevel = 1;

	public static void Log(string message, Object sender, int verboseLevel)
	{
		if (debugLevel >= verboseLevel) 
			Debug.Log(message, sender);
	}
	public static void LogWarning(string message, Object sender, int verboseLevel)
	{
		if(showWarnings)
			Log(message, sender, verboseLevel);
	}
}