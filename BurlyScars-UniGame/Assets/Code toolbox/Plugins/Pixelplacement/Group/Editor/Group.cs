using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Group
{
	static int groupCount = 1;
	static GameObject previousGroup;
	static Dictionary<Transform, Transform> previousChildren;

	[MenuItem("Custom/Group Selected %g")]
	static void MakeGroup()
	{
		Undo.CreateSnapshot();

		//cache selected objects in scene:
		Transform[] selectedObjects = Selection.transforms;

		//early exit if nothing is selected:
		if(selectedObjects.Length == 0)
			return;

		//parent construction and hierarchy structure decision:
		bool nestParent = false;
		Transform _parent = new GameObject("Group " + groupCount++).transform; //naming convention mirrors Photoshop's
		Transform coreParent = selectedObjects[0].parent;
		foreach(Transform item in selectedObjects)
			if(item.parent != coreParent)
			{
				nestParent = false;
				break;
			}
			else
				nestParent = true;
		if(nestParent)
			_parent.parent = coreParent;

		//place group's pivot on the active transform in the scene:
		_parent.position = Selection.activeTransform.position;

		//snapshot for custom undo solution:
		previousGroup = _parent.gameObject;
		previousChildren = new Dictionary<Transform, Transform>();
		foreach(Transform item in selectedObjects)
			previousChildren.Add(item, item.parent);

		//set selected objects as children of the group:
		foreach(Transform item in selectedObjects)
			item.parent = _parent;
	}

	//For some reason Unity refuses to snapshot the creation
	//of a gameobject from an editor script in the undo list
	//so a custom solution is necesary to undo a group's creation
	[MenuItem("Pixelplacement/Group/Undo Last Group %#g")]
	static void UndoLastGroup()
	{
		//early exit if last group was undone or there was no groups were previously created:
		if(previousGroup == null)
			return;
		foreach(KeyValuePair<Transform, Transform> item in previousChildren)
			item.Key.parent = item.Value;
		previousChildren.Clear();
		Object.DestroyImmediate(previousGroup);
	}
}
